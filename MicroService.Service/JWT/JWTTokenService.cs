﻿using System;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using MicroService.IService.JWT;
using MicroService.Entity;
using System.Data;

namespace MicroService.Service
{
    /// <summary>
    /// 提供 Token 服务的实现
    /// </summary>
    public class JWTTokenService : IJWTTokenService
    {
        private readonly IOptionsMonitor<JWTTokenModel> _option;

        /// <summary>
        /// 构造函数 注入
        /// </summary>
        /// <param name="option">注入选项</param>
        public JWTTokenService(IOptionsMonitor<JWTTokenModel> option)
        {
            _option = option;
        }

        /// <summary>
        /// 获取 Token
        /// </summary>
        /// <param name="user">用户信息</param>
        /// <returns></returns>
        public string GetToken(UserModel user)
        {
              #region  //有效载荷
            //var claims = new[]  //鉴别是谁，相关信息
            //{
            //    new Claim(ClaimTypes.Name,user.Name),
            //    new Claim("Id",user.Id.ToString()),
            //    new Claim(ClaimTypes.Role, user.RoleList[0]),
            //    new Claim(ClaimTypes.Role, user.RoleList[1]),
            //    new Claim(ClaimTypes.Email,user.Description)
            //};

            List<Claim> claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name,user.Name),
                new Claim("Id",user.Id.ToString()),             
                new Claim(ClaimTypes.WindowsAccountName,user.Account)
            };
            if (user.Roles != null)
            {
                foreach (var role in user.Roles.Split(','))
                {
                    claims.Add(new Claim(ClaimTypes.Role, role));
                }
            }
            else
            {
                claims.Add(new Claim(ClaimTypes.Role, ""));
            }
            
            #endregion

            //秘钥
            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_option.CurrentValue.SecurityKey!));
            //加密秘钥
            SigningCredentials signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            JwtSecurityToken token = new JwtSecurityToken(
                 issuer: _option.CurrentValue.Issuer!,
                 audience: _option.CurrentValue.Audience!,
                 claims: claims,
                 expires: DateTime.Now.AddMinutes(600),
                 //expires: DateTime.Now.AddSeconds(40),  //设置Token有效时间
                 signingCredentials: signingCredentials
                 );

            string returnToken = new JwtSecurityTokenHandler().WriteToken(token);

            return returnToken;
        }
    }
}
