﻿using MicroService.IService;
using MicroService.Entity;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Service;

/// <summary>
/// 
/// </summary>
public class BaseService : IBaseService
{
    protected ISqlSugarClient Db { get; set; }

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="client"></param>
    public BaseService()
    {
        InitDb();
    }

    void InitDb()
    {
        //Db.DbMaintenance.CreateDatabase();
        Db = new SqlSugarClient(new ConnectionConfig()
        {
            DbType = SqlSugar.DbType.Sqlite,
            ConnectionString = @"DataSource=" + Environment.CurrentDirectory + @"\DataBase\SqlSugar4xTest.sqlite",
            IsAutoCloseConnection = true,
            InitKeyType = InitKeyType.Attribute,
            AopEvents = new AopEvents
            {
                OnLogExecuting = (sql, p) =>
                {
                    Console.WriteLine(sql); //输出sql,查看执行SQL，性能没有影响
                    Console.WriteLine(string.Join(",", p?.Select(it => it.ParameterName + ":" + it.Value)));
                }
            }
        });


    }

    #region  删除
    /// <summary>
    /// 根据主键删除数据 即时commit
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Id"></param>
    public void Delete<T>(int Id) where T : class, new()
    {
        T t = Db.Queryable<T>().InSingle(Id);
        Db.Deleteable(t).ExecuteCommand();
    }

    /// <summary>
    /// 删除数据 即时commit
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t"></param>
    public void Delete<T>(T t) where T : class, new()
    {
        Db.Deleteable(t).ExecuteCommand();
    }
    /// <summary>
    /// 删除数据 即时commit
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="tList"></param>
    public void Delete<T>(List<T> tList) where T : class
    {
        Db.Deleteable(tList).ExecuteCommand();
    }
    #endregion


    #region Query

    public T Find<T>(int id) where T : class
    {
        return Db.Queryable<T>().InSingle(id);
    }

    [Obsolete("尽量避免使用，using带表达式目录树的 代替")]
    public ISugarQueryable<T> Set<T>() where T : class
    {
        return Db.Queryable<T>();
    }

    public ISugarQueryable<T> Queryable<T>(Expression<Func<T, bool>> funcWhere) where T : class
    {
        return Db.Queryable<T>().Where(funcWhere);
    }

    public PagingData<T> QueryPage<T>(Expression<Func<T, bool>> funcWhere, int pageSize, int pageIndex, Expression<Func<T, object>> funcOrderby, bool isAsc = true) where T : class
    {
        var list = Db.Queryable<T>();
        if (funcWhere != null)
        {
            list = list.Where(funcWhere);
        }
        list = list.OrderByIF(true, funcOrderby, isAsc ? OrderByType.Asc : OrderByType.Desc);
        PagingData<T> result = new PagingData<T>()
        {
            DataList=list.ToPageList(pageIndex, pageSize),
            PageIndex=pageIndex,
            PageSize=pageSize,
            RecordCount=list.Count()
        };
        return result;
    }

    #endregion


    #region Update

    public void Update<T>(T t) where T : class, new()
    {
        if(t ==null) throw new Exception("t is null");
        Db.Updateable<T>(t).ExecuteCommand();
    }

    public void Update<T>(List<T> tList) where T : class, new()
    {
        Db.Updateable<T>(tList).ExecuteCommand();
    }
    #endregion

    #region Insert

    public T Insert<T>(T t) where T : class, new()
    {
        Db.Insertable(t).ExecuteCommand();
        return t;
    }

    public IEnumerable<T> Insert<T>(List<T> tList) where T : class, new()
    {
        Db.Insertable<T>(tList.ToList()).ExecuteCommand();
        return tList;
    }




    #endregion


    public ISugarQueryable<T> ExecuteQuery<T>(string sql) where T : class, new()
    {
        return Db.SqlQueryable<T>(sql);
    }






    public void Dispose()
    {
        if(Db!=null)
            Db.Dispose();
    }

}

