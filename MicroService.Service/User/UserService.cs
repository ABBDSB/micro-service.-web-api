﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Security.Principal;
using System.Xml.Linq;
using MicroService.Entity;
using MicroService.IService;
using MicroService.IService.User;
using SqlSugar;

namespace MicroService.Service
{
    public class UserService: BaseService,  IUserService
    {
        private List<UserModel> _userList = new List<UserModel>();

        //public UserService()
        //{
        //    //DataInit();
        //}

        void DataInit()
        {
            
            //_userList.Add(new UserModel()
            //{
            //    Id = 1,
            //    Account = "teacher",
            //    Email = "123456@qq.com",
            //    Name = "teacher",
            //    Password = "123456",
            //    LoginTime = DateTime.Now,
            //    RoleList = new List<string>()
            //    {
            //        Role.Engineer.ToString(),
            //    }
            //});
            //_userList.Add(new UserModel()
            //{
            //    Id = 2,
            //    Account = "root",
            //    Email = "88888@qq.com",
            //    Name = "root",
            //    Password = "123456",
            //    LoginTime = DateTime.Now,
            //    RoleList = new List<string>()
            //    {
            //        //"admin",
            //        //"teacher",
            //        //"student"
            //        Role.Operator.ToString(),
            //        Role.Admin.ToString(),
            //        Role.Engineer.ToString(),
            //        Role.Guest.ToString(),
            //    }
            //});
            //_userList.Add(new UserModel()
            //{
            //    Id = 3,
            //    Account = "student",
            //    Email = "138138438@qq.com",
            //    Name = "student",
            //    Password = "123456",
            //    LoginTime = DateTime.Now,
            //    RoleList = new List<string>()
            //    {
            //        //"admin",
            //        //"teacher",
            //        //"student"
            //         Role.Operator.ToString(),
            //    }
            //});
            //_userList.Add(new UserModel()
            //{
            //    Id = 4,
            //    Account = "admin",
            //    Email = "138138438@qq.com",
            //    Name = "admin",
            //    Password = "123456",
            //    LoginTime = DateTime.Now,
            //    RoleList = new List<string>()
            //    {
            //         Role.Admin.ToString(),
            //        //"admin"
            //        //"teacher",
            //        //"student"
            //    }
            //});
            
            //Db.CodeFirst.InitTables(typeof(UserModel));//Create UserModel 
            //Db.Insertable<List<UserModel>>(_userList).ExecuteCommand();


            
        }

        public IEnumerable<UserModel> FindAll()
        {
            return Db.Queryable<UserModel>().ToList(); // this._userList;
        }

        public UserModel FindUserById(int id)
        {
            return Db.Queryable<UserModel>().First(it => it.Id==id);
            //return this._userList.Find(u => u.Id == id);
        }

        public UserModel FindUserByName(string name)
        {
            return Db.Queryable<UserModel>().First(it => it.Name==name);
            //return this._userList.Find(u => u.Name == name);
        }

      

    }
}
