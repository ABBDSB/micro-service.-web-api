﻿using System;
using MicroService.Entity;

namespace MicroService.IService.JWT;

/// <summary>
/// 提供 Token 的服务
/// </summary>
public interface IJWTTokenService
{
    /// <summary>
    /// 获取 Token
    /// </summary>
    /// <returns></returns>
    string GetToken(UserModel user);
}