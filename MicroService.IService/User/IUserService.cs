﻿using MicroService.Entity;

namespace MicroService.IService.User;

public interface IUserService : IBaseService
{
    UserModel FindUserById(int id);

    UserModel FindUserByName(string name);

    IEnumerable<UserModel> FindAll();
}

