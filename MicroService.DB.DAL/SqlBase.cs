﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.DB.DAL
{
    public abstract class SqlBase
    {
        public abstract SqlSugarClient IniDb(Type DataModel, string DatabaseName, string ServaerName);
    }
}
