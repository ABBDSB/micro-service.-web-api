﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.DB.DAL
{
    public class SqliteConfig
    {
        public static string GetCurrentProjectPath
        {
            get
            {
                return Environment.CurrentDirectory.Replace(@"\bin\Debug", "");
            }
        }
        public static string ConnectionString = @"DataSource=" + GetCurrentProjectPath + @"\DataBase\SqlSugar4xTest.sqlite";
        public static string ConnectionString2 = @"DataSource=" + GetCurrentProjectPath + @"\DataBase\SqlSugar4xTest2.sqlite";
        public static string ConnectionString3 = @"DataSource=" + GetCurrentProjectPath + @"\DataBase\SqlSugar4xTest3.sqlite";
    }
}
