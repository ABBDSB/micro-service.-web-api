﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;

namespace MicroService.DB.DAL
{
    public class DBFactory
    {
        private DBFactory() { }

        static DBFactory() { }
        public static DBFactory Instance { get; }= new DBFactory();

        public SqlBase create(DbType databaseType)
        {
            SqlBase dbFactory ;
            switch(databaseType)
            {
                case DbType.Sqlite :
                    dbFactory = new SqliteServer(); 
                    break;
                case DbType.MySql:
                    dbFactory = new MysqlServer();
                    break;
                default : 
                    dbFactory = new SqliteServer();
                    break;
            }
            return dbFactory;
        }
    }
}
