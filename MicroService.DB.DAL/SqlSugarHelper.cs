﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.DB.DAL
{
    public class SqlSugarHelper
    {
        SqlBase sqlBase;
        SqlSugarClient sqlSugarClient;

        public void IniDB(DbType databaseType, Type dataModel)
        {
            sqlBase = DBFactory.Instance.create(databaseType);
            sqlSugarClient = sqlBase.IniDb(dataModel, "", "");
        }



    }
}
