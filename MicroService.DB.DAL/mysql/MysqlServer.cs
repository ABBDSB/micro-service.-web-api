﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.DB.DAL
{
    public class MysqlServer : SqlBase
    {
        public override SqlSugarClient IniDb(Type DataModel, string DatabaseName, string ServaerName)
        {
            SqlSugarClient db = new SqlSugarClient(new ConnectionConfig()
            {
                ConnectionString = $"server={ServaerName};database={DatabaseName};uid=数据库账号",
                DbType = DbType.MySql,                //设置数据库类型
                IsAutoCloseConnection = true,         //自动释放数据事务
                InitKeyType = InitKeyType.Attribute   //从实体特性中读取主键自增列信息
            }); ;

            db.Aop.OnLogExecuting = (sql, pars) =>
            {
                Console.WriteLine(sql + "\r\n"
                    + db.Utilities.SerializeObject(pars.ToString())); //打印数据库日志
                Console.WriteLine();
            };

            //创建数据库，如果该库不存在，则进行创建
            //db.DbMaintenance.CreateDatabase();

            //初始化数据表，如果没有则创建
            //db.CodeFirst.InitTables(DataModel);

            return db;

        }
    }
}
