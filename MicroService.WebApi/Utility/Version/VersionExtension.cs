﻿namespace MicroService.WebApi.Utility.Version
{
    /// <summary>
    /// APi版本
    /// </summary>
    public static class VersionExtension
    {
        /// <summary>
        /// 配置Core API 文档
        /// </summary>
        /// <param name="builder"></param>
        public static void ApiVersionExt(this WebApplicationBuilder builder)
        {
            //添加 API版本支持
            builder.Services.AddApiVersioning(o =>
            {
                // 是否在响应的 header 信息中返回API版本信息
                o.ReportApiVersions = true;
                // 默认的API版本
                o.DefaultApiVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0);
                // 未指定API版本时，设置API版本为默认的版本
                o.AssumeDefaultVersionWhenUnspecified = true;
            });

            // 配置API版本信息
            builder.Services.AddVersionedApiExplorer(option =>
            {
                //api 版本分组名称
                option.GroupNameFormat = "'v'VVV";
                // 未指定API版本时，设置API版本为默认的版本
                option.AssumeDefaultVersionWhenUnspecified = true;
            });


        }
    }
}
