﻿

using MicroService.Entity;
using SqlSugar;

namespace MicroService.WebApi.Utility.Swagger;

    /// <summary>
    /// Swagger 扩展 一个独立的封装
    /// </summary>
    public static class SwaggerExtension
    {
        /// <summary>
        /// 扩展方法
        /// </summary>
        /// <param name="builder"></param>
        public static void AddSwaggerGenExt(this WebApplicationBuilder builder)
        {

            builder.Services
                .AddControllers(option =>
                {
                    //异常过滤器 全局注册，对于整个项目中所有的方法都生效的
                    option.Filters.Add<CustomExceptionFilterAttribute>();

                    //增加自定义路由前缀
                    option.Conventions.Insert(0, new RouteConvention(new RouteAttribute("api/")));  // 在所有控制器前加前缀api/
                })
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Encoder = JavaScriptEncoder.Create(UnicodeRanges.All); //解决中文乱码的问题
                });

            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(option =>
            {
                #region  自定义支持API分版本展示 -- 的Swagger配置-1
                {
                    //要启用swagger版本控制要在api控制器或方法上添加特性[ApiExplorerSettings(GroupName="版本号")] 这里是枚举
                    typeof(ApiVersions).GetEnumNames().ToList().ForEach(version =>
                    {
                        option.SwaggerDoc(version, new OpenApiInfo()
                        {
                            Title = $"MicroService 接口 {version} Api 文档",
                            Version = version,
                            Description = $"通用版本的 CoreApi 版本 {version}"
                        });
                    });
                }
                #endregion

                #region 组件支持版本展示 调用第三方程序包支持版本控制-1
                {
                    //// 根据API版本信息生成API文档
                    //var provider = builder.Services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();

                    //foreach (var description in provider.ApiVersionDescriptions)
                    //{
                    //    option.SwaggerDoc(description.GroupName, new OpenApiInfo
                    //    {
                    //        Contact = new OpenApiContact
                    //        {
                    //            Name = "Mr.Dayong",
                    //            Email = "1491953921@qq.com"
                    //        },
                    //        Description = ".NET6 WebApi 接口文档",
                    //        Title = ".NET6 WebApi 接口文档",
                    //        Version = description.ApiVersion.ToString()
                    //    });
                    //}

                    ////在 Swagger文档显示前的API地址中将版本信息参数替换位实际的版本号
                    //option.DocInclusionPredicate((version, apiDescription) =>
                    //{
                    //    if (!version.Equals(apiDescription.GroupName))
                    //        return false;

                    //    IEnumerable<string>? values = apiDescription!.RelativePath
                    //    .Split('/')
                    //    .Select(v => v.Replace("v{version}", apiDescription.GroupName));

                    //    apiDescription.RelativePath = string.Join("/", values);
                    //    return true;
                    //});

                    //// 参数使用骆驼峰命名方式
                    option.DescribeAllParametersInCamelCase();
                    //// 取消 API 文档需要输入版本信息
                    option.OperationFilter<RemoveVersionFromParameter>();

                }
                #endregion 

                #region 配置展示注释
                {
                    string projectName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                    //xml文档决定路径
                    var file = Path.Combine(AppContext.BaseDirectory, projectName+".xml"  /*"MicroService.WebApi.xml"*/); //这个 xml文件是当前程序生成的

                    //true: 显示控制器层注释
                    option.IncludeXmlComments(file, true);
                    //对action的名称进行排序，如果有多个，就可以看见效果了
                    option.OrderActionsBy(o => o.RelativePath);

                }
                #endregion

                #region 扩展文件上传按钮
                {
                   option.OperationFilter<FileUploadFilter>();
                }
                #endregion

                #region Swagger 加载Token
                {
                                
                    var security = new OpenApiSecurityRequirement()
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference()
                                {
                                    Id = "Bearer",
                                    Type = ReferenceType.SecurityScheme
                                }
                            }, Array.Empty<string>()
                        }
                    };
                    option.AddSecurityRequirement(security);//添加一个必须的全局安全信息，和AddSecurityDefinition方法指定的方案名称要一致，这里是Bearer。
                    
                    //添加安全定义
                    option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                    {
                        Description = "JWT授权(数据将在请求头中进行传输) 参数结构: \"Authorization: Bearer {token}\"",
                        Name = "Authorization",//jwt默认的参数名称
                        In = ParameterLocation.Header,//jwt默认存放Authorization信息的位置(请求头中)
                        Type = SecuritySchemeType.ApiKey
                    });

                }
                #endregion

            });

            #region 注册 JWT中间件
            {
                builder.Services.Configure<JWTTokenModel>(builder.Configuration.GetSection("JWTTokenOption"));  // 读取配置文件数据
                builder.Services.AddTransient<IJWTTokenService, JWTTokenService>();  // 注册服务 IUserService
                builder.Services.AddTransient<IUserService, UserService>();          // 注册服务 IUserService

               // builder.Services.AddTransient<CustomLogActionFilterAttribute>(); //为了支持ServiceFilter
            }
            #endregion

            #region 添加API版本控制的支持
            {
                //添加 API版本支持
                builder.Services.AddApiVersioning(o =>
                {
                    // 是否在响应的 header 信息中 返回API版本信息
                    o.ReportApiVersions = true;
                    // 默认的API版本
                    o.DefaultApiVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0);
                    // 未指定API版本时，设置API版本为默认的版本
                    o.AssumeDefaultVersionWhenUnspecified = true;
                });

                // 配置API版本信息
                builder.Services.AddVersionedApiExplorer(option =>
                {
                    //api 版本分组名称
                    option.GroupNameFormat = "'v'VVV";
                    // 未指定API版本时，设置API版本为默认的版本
                    option.AssumeDefaultVersionWhenUnspecified = true;
                });
            }
            #endregion
       
            #region  添加log4net
            {
                //配置 log4net ，配置文件为log4net.config
                /*  
                * ILoggerRepository repository = LogManager.CreateRepository("NETCoreRepository");
                * XmlConfigurator.Configure(repository, new FileInfo("log4net.config"));
                */
                builder.Host.ConfigureLogging((context, loggingBuilder) =>
                {
                    MicroService.WebApi.Log.Log4Extension.InitLog4(loggingBuilder);
                });
            }
            #endregion

            #region 添加NLog    
            {
                /* 
                 * var logger = NLog.LogManager.Setup().LoadConfigurationFromFile("NLog.config").GetCurrentClassLogger();
                 * builder.Host.UseNLog();
                 */
                //builder.Services.AddLogging(logging =>
                //{
                //    logging.AddNLog("Log/NLog.config");
                //});
            }
            #endregion

            #region  配置鉴权 
            {
                JWTTokenModel tokenOption = new JWTTokenModel();
                builder.Configuration.Bind("JWTTokenOption", tokenOption);  //读取配置文件appsettings.json 中的 JWTTokenOption项内容           
                //builder.Services.Configure<JWTTokenOption>(builder.Configuration.GetSection("JWTTokenOption"));

                builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(option =>   //这里是配置的鉴权的逻辑
                    {
                        option.TokenValidationParameters = new TokenValidationParameters()
                        {
                            ValidateIssuer = true,      //是否验证 Issuer（发行商）
                            ValidateAudience = true,    //是否验证 Audience （受众者）
                            ValidateLifetime = true,    //是否验证失效时间
                            ValidateIssuerSigningKey = true, //是否验证 Issuer的签名键
                            ValidAudience = tokenOption.Audience,
                            ValidIssuer = tokenOption.Issuer, //VaildAudience, ValidIssuer这两项的值要和验证中心的值保持一致
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenOption.SecurityKey!)),
                            //AudienceValidator = (m, n, z) =>  //可以添加一些自定义的动作
                            //{
                            //    return true;
                            //},
                            //LifetimeValidator = (notBefore, expires, securityToken, validationParameters) =>
                            //{
                            //    return true;
                            //}
                        };
                    });
            }
            #endregion

            #region 允许跨域的规则配置
            {
                builder.Services.AddCors(options =>
                {
                    options.AddPolicy("Cors", policy =>
                    {
                        policy.AllowAnyOrigin();
                        policy.AllowAnyHeader();
                        policy.AllowAnyMethod();
                    });
                });
            }
            #endregion

            #region 初始化Autofac 注入程序集
            //builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
            //var hostBuilder = builder.Host.ConfigureContainer<ContainerBuilder>(builder =>
            //{
            //    var assembly = Assembly.Load("NET7.Infrastructure");
            //    builder.RegisterAssemblyTypes(assembly).Where(a => a.Name.EndsWith("Repository")).AsSelf();
            //});
            #endregion

            // builder.Services.AddMemoryCache();  //注入系统缓存

            #region 配置为本机默认IP模式
            {
                // 建议后续改到 配置文件中更好
                //builder.WebHost.UseUrls("http://*:8711;https://*:8701");// 这里可以是数组，表示启动多个端口

                string urls= builder.Configuration.GetSection("Url").Value;  //从配置文件 appsettings.json 中读取 Url 项内容    
                builder.WebHost.UseUrls(urls);
            }
            #endregion


            #region SqlSugar
            {
                //注册上下文：AOP里面可以获取IOC对象，如果有现成框架比如Furion可以不写这一行
                builder.Services.AddHttpContextAccessor();
                //注册SqlSugar
                builder.Services.AddSingleton<ISqlSugarClient>(s =>
                {
                    SqlSugarScope sqlSugar = new SqlSugarScope(new ConnectionConfig()
                    {
                        DbType = SqlSugar.DbType.Sqlite,
                        ConnectionString = @"DataSource=" + Environment.CurrentDirectory + @"\DataBase\SqlSugar4xTest.sqlite",                     
                        IsAutoCloseConnection = true,
                        InitKeyType = InitKeyType.Attribute,
                    },
                   db =>
                   {
                       //单例参数配置，所有上下文生效
                       db.Aop.OnLogExecuting = (sql, pars) =>
                       {
                           //获取作IOC作用域对象
                           //var appServive = s.GetService<IHttpContextAccessor>();
                           //var obj = appServive?.HttpContext?.RequestServices.GetService<Log>();
                           //Console.WriteLine("AOP" + obj.GetHashCode());
                           Console.WriteLine(sql);
                           Console.WriteLine(string.Join(",", pars?.Select(it => it.ParameterName + ":" + it.Value)));
                       };
                   });
                    return sqlSugar;
                });
            }
            #endregion


        }

        /// <summary>
        /// swagger中间件配置应用
        /// </summary>
        /// <param name="app"></param>
        public static void UseSwaggerExt(this WebApplication app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(option =>
            {
                #region 自定义支持API分版本展示  --的Swagger配置-2
                {
                    foreach (string version in typeof(ApiVersions).GetEnumNames())
                    {
                        option.SwaggerEndpoint($"/swagger/{version}/swagger.json", $"API版本：{version}");
                    }
                    option.DefaultModelExpandDepth(-1); //不显示Models
                }
                #endregion


                #region 调用第三方程序包支持版本控制-2
                {
                    //var provider = app.Services.GetRequiredService<IApiVersionDescriptionProvider>();
                    ////默认加载最新版本的API文档
                    //foreach (var description in provider.ApiVersionDescriptions.Reverse())
                    //{
                    //    option.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json",
                    //        $"API_{description.GroupName.ToUpperInvariant()}");
                    //}
                }
                #endregion

            });

            app.UseCors("Cors");// 允许跨域，并使用上述配置的Cors规则。添加时注意顺序，要加在其他服务之前

            #region 使用鉴权授权中间件
            {
                app.UseAuthentication();   //使用鉴定/认证
                app.UseAuthorization();    //使用授权 启用权限验证中间件
            }
            #endregion

        }


        /// <summary>
        /// 获取所有的控制器
        /// </summary>
        public static List<Type> GetControllers()
        {
            Assembly asm = Assembly.GetExecutingAssembly();

            var controlleractionlist = asm.GetTypes()
                .Where(type => typeof(ControllerBase).IsAssignableFrom(type))
                .OrderBy(x => x.Name).ToList();
            return controlleractionlist;
        }

        /// <summary>
        /// 获取控制器对应的swagger分组值
        /// </summary>
        public static string GetSwaggerGroupName(Type controller)
        {
            var groupname = controller.Name.Replace("Controller", "");
            var apisetting = controller.GetCustomAttribute(typeof(ApiExplorerSettingsAttribute));
            if (apisetting != null)
            {
                groupname = ((ApiExplorerSettingsAttribute)apisetting).GroupName;
            }

            return groupname;
        }

    }

