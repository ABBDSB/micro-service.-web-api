﻿
namespace MicroService.WebApi.Utility.Swagger
{
    /// <summary>
    /// 移除版本参数
    /// </summary>
    public class RemoveVersionFromParameter :  IOperationFilter
    {
        /// <summary>
        /// 扩展 
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="context"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var versionParameter = operation.Parameters.FirstOrDefault(p => p.Name == "api-version");
            //var versionParameter = operation.Parameters.FirstOrDefault(p => p.Name == "version");
            if (versionParameter != null)
            {
                operation.Parameters.Remove(versionParameter);
            }
        }
    }
}
