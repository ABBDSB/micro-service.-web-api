﻿namespace MicroService.WebApi.Utility.Swagger
{
    /// <summary>
    /// 版本枚举
    /// </summary>
    public enum ApiVersions
    {
        /// <summary>
        /// 第一版本
        /// </summary>
        V1,

        /// <summary>
        /// 第二版本
        /// </summary>
        V2,

        /// <summary>
        /// 第三版本
        /// </summary>
        V3,

        /// <summary>
        /// 第四版本
        /// </summary>
        V4
    }
}
