﻿

namespace MicroService.WebApi.Utility.Swagger
{
    /// <summary>
    /// 扩展文件上传,展示选择文件的按钮
    /// </summary>
    public class FileUploadFilter : IOperationFilter
    {
        /// <summary>
        /// 文件上传筛选
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="context"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            const string FileUploadContentType = "multipart/form-data";  //只针对这种类型
            if (operation.RequestBody == null || !operation.RequestBody.Content.Any(x => x.Key.Equals(FileUploadContentType, StringComparison.InvariantCultureIgnoreCase)))
            {
                return;
            }

            if (context.ApiDescription.ParameterDescriptions[0].Type == typeof(IFormCollection))
            {
                operation.RequestBody = new OpenApiRequestBody
                {
                    Description = "文件上传",
                    Content = new Dictionary<string, OpenApiMediaType>
                    {
                        {
                            FileUploadContentType,new OpenApiMediaType
                            {
                                Schema=new OpenApiSchema
                                {
                                    Type="object",
                                    Required=new HashSet<string>{"file"},
                                    Properties=new Dictionary<string, OpenApiSchema>
                                    {
                                        {
                                            "file",new OpenApiSchema()
                                            {
                                                Type="string",
                                                Format="binary"
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                };
            }

        }
    }
}
