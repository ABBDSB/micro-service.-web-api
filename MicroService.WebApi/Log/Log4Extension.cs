﻿/*
 * 需要安装依赖包： Microsoft.Extensions.Logging.Log4Net.AspNetCore
 * 如使用SQLite 数据库需要安装依赖包：System.Data.SQLite 
 * log4net.config 中配置的SQlite数据库路径需要为绝对路径
 */

namespace MicroService.WebApi.Log
{
    /// <summary>
    /// log4扩展类
    /// </summary>
    public static class Log4Extension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="loggingBuilder"></param>
        public static void InitLog4(ILoggingBuilder loggingBuilder)
        {
            //loggingBuilder.AddFilter("System", LogLevel.Warning);
            //loggingBuilder.AddFilter("Microsoft", LogLevel.Warning); //过滤掉系统默认的一些日志
            //loggingBuilder.AddFilter("System", LogLevel.Information);
            loggingBuilder.AddLog4Net(new Log4NetProviderOptions()
            {
                Log4NetConfigFileName = "Log/log4net.config",
                Watch = true
            });
        }
    }
}
