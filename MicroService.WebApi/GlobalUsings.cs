﻿global using MicroService.Entity;
global using MicroService.Service;

global using MicroService.IService.JWT;
global using MicroService.IService.User;

global using MicroService.WebApi.Utility.Route;
global using MicroService.WebApi.Utility.Swagger;

global using MicroService.Quartz;
global using MicroService.WebApi.Filters;

global using System.Text;
global using System.Text.Encodings.Web;
global using System.Text.Unicode;
global using System.Text.RegularExpressions;

global using Microsoft.AspNetCore.Mvc;
global using Microsoft.AspNetCore.Mvc.ApplicationModels;
global using Microsoft.AspNetCore.Mvc.Routing;

global using Microsoft.AspNetCore.Authentication.JwtBearer;
global using Microsoft.AspNetCore.Authorization;
global using Microsoft.AspNetCore.Mvc.Filters;
global using Microsoft.IdentityModel.Tokens;
global using Microsoft.OpenApi.Models;

global using System.Data;
global using System.Reflection;

global using Newtonsoft.Json;

global using Swashbuckle.AspNetCore.SwaggerGen;


global using SqlSugar;