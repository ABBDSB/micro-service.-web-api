
try
{
    Console.WriteLine("------------- MicroService.WebApi is Starting -------------");
    Console.WriteLine($"------------- StartTime:{DateTime.Now.ToString("yyyy-mm-dd HH:mm:ss")} ---------------");
    Console.WriteLine("-----------------------------------------------------------");

    string value =Role.Admin.ToString();
    var re = Role.Admin.GetType().GetField(value);

    //{
    //     var basepath=  AppDomain.CurrentDomain.BaseDirectory;
    //    Console.WriteLine("Path:" + AppDomain.CurrentDomain.BaseDirectory);
    //}

    var builder = WebApplication.CreateBuilder(args);

    builder.AddSwaggerGenExt(); //自定义扩展



   
    var app = builder.Build();

    app.UseSwaggerExt(); // 自定义扩展

    app.UseHttpsRedirection();

    app.MapControllers();

    app.Run();

}
catch(Exception ex)
{
    Console.WriteLine(ex.Message);
}




