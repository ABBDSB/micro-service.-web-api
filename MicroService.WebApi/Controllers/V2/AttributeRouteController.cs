﻿namespace MicroService.WebApi.Controllers.V2
{

    /// <summary>
    /// 特性路由解读
    /// </summary>  
    [ApiController]
    //[ApiVersion("2.0")]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = nameof(ApiVersions.V2))]  //自定义API版本
    [Route("[controller]/[action]")]                //路由设置
    [Authorize(Roles = "Admin,Engineer,Operator,Guest")]    //角色权限管控
    public class AttributeRouteController : ControllerBase
    {
        private readonly ILogger<AttributeRouteController> _logger;
        private readonly IUserService _userService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public AttributeRouteController(ILogger<AttributeRouteController> logger, IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }

        /// <summary>
        /// 获取所有用户信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetUserAll()
        {
            try
            {
                var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
                {
                    State = true,
                    Message = "",
                    Data = _userService.FindAll()
                });
                _logger.LogInformation(result);
                await Task.Delay(10);
                return Content(result);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Error: {ex.ToString()}");
            }

            _logger.LogInformation($"查询失败");
            return Content(JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = false,
                Message = "查询失败"
            }));
        }

        /// <summary>
        /// 根据Id获取用户信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{userId:int}")] // 增加了路由约束 userId 必须是int类型，不然报404
        public async Task<IActionResult> GetUserById(int userId)
        {
            try
            {
                var user = _userService.FindUserById(userId);
                if (user != null)
                {
                    var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
                    {
                        State = true,
                        Message = "",
                        Data = user
                    });
                    _logger.LogInformation(result);
                    await Task.Delay(10);
                    return Content(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Error: {ex.ToString()}");
            }

            _logger.LogInformation($"查询失败");
            return Content(JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = false,
                Message = "查询失败"
            }));
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="keyWorks"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{pageIndex}/{pageSize}/{keyWorks}")]
        public async Task<IActionResult> GetPageUser(int pageIndex, int pageSize, string keyWorks)
        {
            try
            {
                var user = _userService.FindUserById(1);
                if (user != null)
                {
                    var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
                    {
                        State = true,
                        Message = "",
                        Data = user
                    });
                    _logger.LogInformation(result);
                    await Task.Delay(10);
                    return Content(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Error: {ex.ToString()}");
            }

            _logger.LogInformation($"查询失败");
            return Content(JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = false,
                Message = "查询失败"
            }));
        }

        /// <summary>
        /// 新增信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("TestAddUser")]
        public async Task<IActionResult> AddUser(UserModel user)
        {
            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = user
            });
            _logger.LogInformation(result);
            await Task.Delay(10);
            return Content(result);
        }

        /// <summary>
        /// 布尔类型约束
        /// </summary>
        /// <param name="isOk"></param>
        /// <returns></returns>
        [HttpGet()]
        [Route("BoolConstraint/{isOk:bool}")]
        public async Task<IActionResult> BoolConstraint(bool isOk)
        {
            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = "IsOk:" + isOk
            });
            _logger.LogInformation(result);
            await Task.Delay(10);
            return Content(result);
        }

        /// <summary>
        /// 时间类型约束
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        [HttpGet()]
        [Route("DatetimeConstraint/{date:DateTime}")]
        public async Task<IActionResult> DateTimeConstraint(DateTime date)
        {
            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = "date:" + DateTime.Now.ToString("dddd-MM-dd HH:mm:ss")
            });
            _logger.LogInformation(result);
            await Task.Delay(10);
            return Content(result);
        }


    }

}

