﻿

namespace MicroService.WebApi.Controllers.V2
{
    /// <summary>
    /// 框架提供的版本控制
    /// </summary>
    [ApiController]
    //[ApiVersion("2.0")]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = nameof(ApiVersions.V2))]  //自定义API版本
    [Route("[controller]/{ApiVersions.V2}/[action]")]  //路由设置    ApiVersions.V2会变成参数，这样路由会根据参数不同而不同
    [Authorize(Roles = "Admin,Engineer,Operator,Guest")]    //角色权限管控
    public class VersionControlController : ControllerBase
    {
        private readonly ILogger<VersionControlController> _logger;
        private readonly IUserService _userService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="_userService"></param>
        public VersionControlController(ILogger<VersionControlController> logger, IUserService  _userService)
        {
            this._logger = logger;
            this._userService = _userService;
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomCacheResourceFilter]
        public async Task<IActionResult> GetUserAll()
        {
            try
            {
                var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
                {
                    State = true,
                    Message="",
                    Data = _userService.FindAll()
                });
                _logger.LogInformation(result);
                await Task.Delay(10);
                return Content(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }
            _logger.LogInformation($"查询失败");
            return Content(JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = false,
                Message = "查询失败"
            }));
        }
    }
}
