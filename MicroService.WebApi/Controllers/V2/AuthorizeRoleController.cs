﻿

namespace MicroService.WebApi.Controllers.V2
{

    /// <summary>
    /// 角色管控资源解析
    /// </summary>
    [ApiController]
    //[ApiVersion("2.0")]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = nameof(ApiVersions.V2))]  //自定义API版本
    [Route("[controller]/[action]")]  //路由设置   
    //[Authorize(Roles = "admin,teacher,student")]       //角色权限管控 表示只需满足 admin、student、teacher 三个角色权限之一即可
    //[Authorize(Roles = "admin")]                       //角色权限管控 表示同时满足 admin、student、teacher角色权限
    //[Authorize(Roles = "student")]
    //[Authorize(Roles = "teacher")]
    public class AuthorizeRoleController : ControllerBase
    {
        private readonly ILogger<AuthorizeRoleController> _logger;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public AuthorizeRoleController(ILogger<AuthorizeRoleController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// 获取时间 需同时满足 Admin,Engineer,Operator,Guest //角色权限管控角色权限
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Admin")]       //角色权限管控 表示同时满足 Admin,Engineer,Operator,Guest角色权限
        [Authorize(Roles = "Engineer")]
        [Authorize(Roles = "Operator")]
        public async Task<IActionResult> GetInfo()
        {
            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = $"Time: {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}"
            });
            _logger.LogInformation(result);
            await Task.Delay(10);
            return Content(result);          
        }

        /// <summary>
        ///  获取时间 只需满足 admin、student、teacher 三个角色权限之一即可
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Admin,Engineer,Operator,Guest")]       //角色权限管控 表示只需满足 admin、student、teacher 三个角色权限之一即可
        public async Task<IActionResult> GetInfo2()
        {
            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = $"Time: {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}"
            });
            _logger.LogInformation(result);
            await Task.Delay(10);
            return Content(result);
        }


    }
}
