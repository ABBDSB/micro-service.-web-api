﻿
namespace MicroService.WebApi.Controllers.V2
{
    /// <summary>
    /// 文件资源
    /// </summary>
    [ApiController]
    [Route("[controller]/[action]")]
    //[ApiVersion("2.0")]                                                         //
    [ApiExplorerSettings(IgnoreApi = false, GroupName = nameof(ApiVersions.V2))]  //自定义API版本
    [Authorize(Roles = "Admin,Engineer,Operator,Guest")]    //角色权限管控
    public class FileController : ControllerBase
    {
        private readonly ILogger<FileController> _logger;

        

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="logger"></param>
        public FileController(ILogger<FileController> logger)
        {
            _logger = logger;           
        }

      

        /// <summary>
        /// 图片上传
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UploadImage(IFormFile image)
        {
            if (image == null || image.Length == 0)
                return BadRequest("未选择文件");
            // 获取文件名和扩展名
            var fileName = Path.GetFileNameWithoutExtension(image.FileName);
            var fileExt = Path.GetExtension(image.FileName);
            // 生成唯一的文件名
            fileName = fileName + "_" + DateTime.Now.Ticks.ToString() + fileExt;

            // 保存文件到文件系统
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "uploads", fileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await image.CopyToAsync(fileStream);
            }

            // 保存文件到数据库
            //var imageModel = new ImageModel
            //{
            //    FileName = fileName,
            //    ContentType = image.ContentType,
            //    Content = new byte[image.Length]
            //};
            //using (var memoryStream = new MemoryStream(imageModel.Content))
            //{
            //    await image.CopyToAsync(memoryStream);
            //}
            ////保存到数据库中，这儿用的是sqlsugar也可以换成ef core
            //await Db.Insertable(imageModel).ExecommandAsync();

            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = image.FileName+"：上传成功",
                Data = ""
            });
            _logger.LogInformation(result);
            await Task.Delay(10);
            return Content(result);
        }

        ///// <summary>
        ///// 文件上传
        ///// </summary>
        ///// <param name="from"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public JsonResult UploadFile(IFormCollection from)
        //{
        //    _logger.LogInformation($"文件: {from.Files.FirstOrDefault()?.FileName}，上传成功");
        //    return new JsonResult(new
        //    {
        //        Success = true,
        //        Message = "上传成功",
        //        from.Files.FirstOrDefault()?.FileName
        //    });
        //}

        /// <summary>
        /// 文件上传 (大小限制100M以内)
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        [RequestSizeLimit(100_000_000)] // 请求文件大小限制 100M左右  默认30M
        public async Task<ActionResult> UploadFile2(IFormFile file)//上传文件
        {
            var fileData = new byte[file.Length];
            using (var ms = new MemoryStream())
            {
                await file.CopyToAsync(ms);
                fileData = ms.ToArray();
            }

            CacheData.mOperFile = new OperFile
            {
                FileName = file.FileName,
                FileType = file.ContentType,
                FileSize = file.Length,
                FileData = fileData
            };
            ////插入数据库方法
            //await _db.Insertable(myFile).ExecommandAsync();//示例是SqlSugar，也可以用EF CORE
            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = file.FileName + "：上传成功",
                Data = ""
            });
            _logger.LogInformation(result);
            await Task.Delay(10);
            return Content(result);
        }


        /// <summary>
        /// 文件下载
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Download()//下载文件
        {
            //查询存储的文件信息
            //var myFile = await _db.Queryable<MyFiles>().FirstAsync(f => f.Id == id);
            //if (myFile == null)
            //{
            //    return NotFound();
            //}
            //return File(myFile.FileData, myFile.FileType, myFile.FileName);

            if(CacheData.mOperFile == null)
            {
                return NotFound();
            }
            await Task.Delay(10);
            return File(CacheData.mOperFile.FileData, CacheData.mOperFile.FileType, CacheData.mOperFile.FileName);
        }


    }

    /// <summary>
    /// 上传文件类
    /// </summary>
    public class OperFile
    {
        public string FileName { get; set; }

        public string FileType { get; set; }

        public long FileSize { get; set; }

        public byte[] FileData { get; set; }
    }
    /// <summary>
    /// 文件数据
    /// </summary>
    public class CacheData
    {
        public static OperFile mOperFile;
    }

}
