﻿
namespace MicroService.WebApi.Controllers.V1.Token
{
    /// <summary>
    /// 获取Token
    /// </summary>  
    [ApiController]
    [Route("[controller]/[action]")]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = nameof(ApiVersions.V1))]  //自定义API版本
    public class AuthenticationCenterController : ControllerBase
    {
        private readonly ILogger<AuthenticationCenterController> _logger;
        private readonly IJWTTokenService _JWTTokenService;  //使用前需要先注册 IJWTTokenService 服务/接口
        private readonly IUserService _UserService;          //使用前需要先注册 IUserService 服务/接口

        /// <summary>
        /// 构造函数 注入服务
        /// </summary>      
        public AuthenticationCenterController(IJWTTokenService iJWTTokenService, IUserService userService, ILogger<AuthenticationCenterController> logger)
        {
            _JWTTokenService = iJWTTokenService;
            _UserService = userService;
            _logger = logger;
            
        }

        /// <summary>
        /// 登入 获取token信息
        /// </summary>
        /// <param name="username">用户名</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromForm] string username, [FromForm] string password)
        {
            try
            {
                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    //从数据库中查询出来的
                    var _user = _UserService.FindUserByName(username);
                    if (_user != null && string.Compare(_user.Name, username, true) == 0 && string.Compare(_user.Password, password, true) == 0)
                    {
                        string token = _JWTTokenService.GetToken(_user);
                        var result1 = JsonConvert.SerializeObject(new ApiResponseResultModel()
                        {
                            State = true,
                            Message = "",
                            Data = token
                        });
                     
                        _logger.LogInformation($"{username}_{password} 获取Token成功");
                        await Task.Delay(10);
                        return Content(result1);
                    }
                }                        
            }
            catch (Exception ex)
            {
                _logger.LogError($"{username}_{password} 获取Token报错：{ex.ToString()}");             
            }
            _logger.LogInformation($"{username}_{password} 获取Token失败");
            var result2 = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = false,
                Message = "获取Token失败"
            });
            return Content(result2);
        }


    }
}
