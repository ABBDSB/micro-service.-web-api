﻿

namespace MicroService.WebApi.Controllers.V3
{
    /// <summary>
    /// Log记录测试接口
    /// </summary>
    [ApiController]
    //[ApiVersion("3.0")]
    [Route("[controller]/[action]")]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = nameof(ApiVersions.V3))]  //自定义API版本
    [Authorize(Roles = "Admin,Engineer,Operator,Guest")]    //角色权限管控
    public class LogActionFilterController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        // [CustomCacheResourceFilter]
        [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        // [ServiceFilter(typeof(CustomLogActionFilterAttribute))]
        // [TypeFilter(typeof(CustomExceptionFilterAttribute))]
        [Produces("application/json")]  //声明控制器的操作支持 application/json 的响应内容类型
        public async Task<IActionResult> GetName()
        {
            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = DateTime.Now.ToString("HH:mm:ss") + " : 广州恒大"
            });
            await Task.Delay(10);
            return Content(result) ;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        // [CustomCacheResourceFilter]
        [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        //[ServiceFilter(typeof(CustomLogActionFilterAttribute))]
        // [TypeFilter(typeof(CustomExceptionFilterAttribute))]
        public async Task<IActionResult> QueryNameById([FromForm] int id)
        {
            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = DateTime.Now.ToString("HH:mm:ss") + " 查询ID: " + id
            });
            await Task.Delay(10);
            return Content(result);
        }

        /// <summary>
        /// 
        /// </summary>
        [HttpPost]
        // [CustomCacheResourceFilter]
        [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        //[ServiceFilter(typeof(CustomLogActionFilterAttribute))]
        // [TypeFilter(typeof(CustomExceptionFilterAttribute))]
        public async Task<IActionResult> QueryInfo([FromForm] int id, [FromForm] string name)
        {
            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = DateTime.Now.ToString("HH:mm:ss") + $" 查询ID:{id} 姓名：{name} "
            });
            await Task.Delay(10);
            return Content(result);
        }


    }
}
