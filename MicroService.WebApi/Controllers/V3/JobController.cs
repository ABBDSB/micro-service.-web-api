﻿

namespace MicroService.WebApi.Controllers.V3
{
    /// <summary>
    /// 任务调度
    /// </summary>
    [ApiController]
    //[ApiVersion("3.0")]
    [Route("[controller]/[action]")]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = nameof(ApiVersions.V3))]  //自定义API版本
    [Produces("application/json")]  //声明控制器的操作支持 application/json 的响应内容类型
    [Authorize(Roles = "Admin,Engineer,Operator,Guest")]    //角色权限管控
    public class JobController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CustomCacheResourceFilter]
        // [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        // [ServiceFilter(typeof(CustomLogActionFilterAttribute))]
        // [TypeFilter(typeof(CustomExceptionFilterAttribute))]       
        public async Task<IActionResult> SetJob()
        {
            MicroService.Quartz.DispatchingManager.Init().GetAwaiter().GetResult();


            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = DateTime.Now.ToString("HH:mm:ss") + " : 西门飞雪"
            });
            await Task.Delay(10);
            return Content(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        // [CustomCacheResourceFilter]
        // [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        // [ServiceFilter(typeof(CustomLogActionFilterAttribute))]
        // [TypeFilter(typeof(CustomExceptionFilterAttribute))]       
        public async Task<IActionResult> SetJob2()
        {
            MicroService.Quartz.DispatchingManager.Init2().GetAwaiter().GetResult();


            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = DateTime.Now.ToString("HH:mm:ss") + " : 西门飞雪-2"
            });
            await Task.Delay(10);
            return Content(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        // [CustomCacheResourceFilter]
        // [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        // [ServiceFilter(typeof(CustomLogActionFilterAttribute))]
        // [TypeFilter(typeof(CustomExceptionFilterAttribute))]       
        public async Task<IActionResult> SetJob3()
        {
            MicroService.Quartz.DispatchingManager.Init3().GetAwaiter().GetResult();


            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = DateTime.Now.ToString("HH:mm:ss") + " : 西门飞雪-3"
            });
            await Task.Delay(10);
            return Content(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        // [CustomCacheResourceFilter]
        // [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        // [ServiceFilter(typeof(CustomLogActionFilterAttribute))]
        // [TypeFilter(typeof(CustomExceptionFilterAttribute))]       
        public async Task<IActionResult> SetJob4()
        {
            MicroService.Quartz.DispatchingManager.Init4().GetAwaiter().GetResult();


            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = DateTime.Now.ToString("HH:mm:ss") + " : 西门飞雪-4"
            });
            await Task.Delay(10);
            return Content(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        // [CustomCacheResourceFilter]
        // [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        // [ServiceFilter(typeof(CustomLogActionFilterAttribute))]
        // [TypeFilter(typeof(CustomExceptionFilterAttribute))]       
        public async Task<IActionResult> SetJob5()
        {
            MicroService.Quartz.DispatchingManager.Init5().GetAwaiter().GetResult();


            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = DateTime.Now.ToString("HH:mm:ss") + " : 西门飞雪-5"
            });
            await Task.Delay(10);
            return Content(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        // [CustomCacheResourceFilter]
        // [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        // [ServiceFilter(typeof(CustomLogActionFilterAttribute))]
        // [TypeFilter(typeof(CustomExceptionFilterAttribute))]       
        public async Task<IActionResult> SetJob6()
        {
            MicroService.Quartz.DispatchingManager.Init6().GetAwaiter().GetResult();


            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = DateTime.Now.ToString("HH:mm:ss") + " : 西门飞雪-6"
            });
            await Task.Delay(10);
            return Content(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        // [CustomCacheResourceFilter]
        // [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        // [ServiceFilter(typeof(CustomLogActionFilterAttribute))]
        // [TypeFilter(typeof(CustomExceptionFilterAttribute))]       
        public async Task<IActionResult> SetJob7()
        {
            MicroService.Quartz.DispatchingManager.Init7().GetAwaiter().GetResult();


            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = DateTime.Now.ToString("HH:mm:ss") + " : 西门飞雪-7"
            });
            await Task.Delay(10);
            return Content(result);
        }


    }
}
