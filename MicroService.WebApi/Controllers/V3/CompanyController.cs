﻿
namespace MicroService.WebApi.Controllers.V3;

/// <summary>
/// 公司资源
/// </summary>
[ApiController]
//[ApiVersion("3.0")]
[Route("[controller]/[action]")]
[ApiExplorerSettings(IgnoreApi = false, GroupName = nameof(ApiVersions.V3))]  //自定义API版本
[Produces("application/json")]  //声明控制器的操作支持 application/json 的响应内容类型
[Authorize(Roles = "Admin,Engineer,Operator,Guest")]    //角色权限管控
public class CompanyController : ControllerBase
{
      
    /// <summary>
    /// 获取公司信息
    /// </summary>
    [HttpPost]
    // [CustomCacheResourceFilter]
        [TypeFilter(typeof(CustomLogActionFilterAttribute))]
    // [ServiceFilter(typeof(CustomLogActionFilterAttribute))]
    public async Task<IActionResult> GetCompany()
    {
        var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
        {
            State=true,
            Message="",
            Data= DateTime.Now.ToString("HH:mm:ss") + " : 四川成都西门子"
        });
        await Task.Delay(10);
        return Content(result);           
    }

    /// <summary>
    /// 新增公司信息 student
    /// </summary>
    [HttpPost]
    public async Task<IActionResult> AddCompany([FromBody] UserModel user)
    {
        var result= JsonConvert.SerializeObject(new ApiResponseResultModel()
        {
            State = true,
            Message = DateTime.Now.ToString("HH:mm:ss"),
            Data = user
        });
        await Task.Delay(10);
        return Content(result);
    }

    /// <summary>
    /// 修改公司信息  teacher
    /// </summary>
    [HttpPut]
    public async Task<IActionResult> UpdateCompany([FromBody] UserModel user)
    {
        var result= JsonConvert.SerializeObject(new ApiResponseResultModel()
        {
            State = true,
            Message = DateTime.Now.ToString("HH:mm:ss"),
            Data = user
        });
        await Task.Delay(10);
        return Content(result);
    }

    /// <summary>
    /// 删除公司信息
    /// </summary>
    [HttpDelete]
    public async Task<IActionResult> DeleteCompany([FromBody] UserModel user)
    {
        var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
        {
            State = true,
            Message = DateTime.Now.ToString("HH:mm:ss"),
            Data = user 
        });
        await Task.Delay(10);
        return Content(result);
    }

    /// <summary>
    /// 不管控权限
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> GetInfo()
    {
        var result= JsonConvert.SerializeObject(new ApiResponseResultModel()
        {
            State = true,
            Message = "",
            Data= "GetInfo: "+ DateTime.Now.ToString("HH:mm:ss")
        });
        await Task.Delay(10);
        return Content(result);
    }


    /// <summary>
    /// 修改公司信息  teacher
    /// </summary>
    [HttpPost]
    public async Task<IActionResult> Get2Company([FromForm] string  name)
    {
        var result= JsonConvert.SerializeObject(new ApiResponseResultModel()
        {
            State = true,
            Message = DateTime.Now.ToString("HH:mm:ss"),
            Data = name
        });
        await Task.Delay(10);
        return Content(result);
    }

}

