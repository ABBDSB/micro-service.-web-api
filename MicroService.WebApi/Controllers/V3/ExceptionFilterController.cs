﻿

namespace MicroService.WebApi.Controllers.V3
{
    /// <summary>
    /// 异常处理测试接口
    /// </summary>
    [ApiController]
    //[TypeFilter(typeof(CustomExceptionFilterAttribute))] //控制器注册,当前控制器下所有方法都会生效
    //[ApiVersion("3.0")]
    [Route("[controller]/[action]")]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = nameof(ApiVersions.V3))]  //自定义API版本
    [Produces("application/json")]  //声明控制器的操作支持 application/json 的响应内容类型
    [Authorize(Roles = "Admin,Engineer,Operator,Guest")]    //角色权限管控
    public class ExceptionFilterController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        // [CustomCacheResourceFilter]
        // [TypeFilter(typeof(CustomExceptionFilterAttribute))]  //方法注册
        // [ServiceFilter(typeof(CustomLogActionFilterAttribute))] //需要在IOC容器中注册CustomLogActionFilterAttribute 这样才能使用，在program.cs中  builder.Services.AddTransient<CustomLogActionFilterAttribute>

        // [TypeFilter(typeof(CustomExceptionFilterAttribute))]
        public async Task<IActionResult> GetName()
        {
            {
                int a = 3;
                int b = 0;
                var c = a / b;  //此处报错
            }
            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State=true,
                Message="",
                Data= DateTime.Now.ToString("HH:mm:ss") + "红红火火恍恍惚惚"
            });
            await Task.Delay(10);
            return Content(result);
        }


       

    }
}
