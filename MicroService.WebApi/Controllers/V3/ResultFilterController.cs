﻿

namespace MicroService.WebApi.Controllers.V3
{
    /// <summary>
    ///  ResultFilter/AlwaysRunResultFilter
    /// </summary>
    [ApiController]
    //[ApiVersion("3.0")]
    [Route("[controller]/[action]")]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = nameof(ApiVersions.V3))]  //自定义API版本
    [Produces("application/json")]  //声明控制器的操作支持 application/json 的响应内容类型
    [Authorize(Roles = "Admin,Engineer,Operator,Guest")]    //角色权限管控
    public class ResultFilterController : ControllerBase
    {
        /// <summary>
        /// 测试 ResultFilter
        /// </summary>
        [HttpPost]
        [CustomResultFilterAttribute]
        [Produces("application/json")]
        public async Task<IActionResult> ResultFilterApi(int id)
        {
            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = DateTime.Now.ToString("HH:mm:ss") + " __  "+id
            });
            await Task.Delay(10);
            return Content(result);
        }


        /// <summary>
        /// 测试 AlwaysRunResultFilter
        /// </summary>
        [HttpPost]
        [CustomResultFilterAttribute]
        [CustomAlwaysRunResultFilterAttribute]
        public async Task<IActionResult> ResultFilterApiTwo(int id)
        {
            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = DateTime.Now.ToString("HH:mm:ss") + " __  " + id
            });

            await Task.Delay(10);
            return Content(result);
        }
    }
}
