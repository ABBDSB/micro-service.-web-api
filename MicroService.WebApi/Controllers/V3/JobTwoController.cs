﻿
namespace MicroService.WebApi.Controllers.V3
{

    /// <summary>
    /// API任务调度
    /// </summary>
    [ApiController]
    //[ApiVersion("3.0")]
    [Route("[controller]/[action]")]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = nameof(ApiVersions.V3))]  //自定义API版本
    [Produces("application/json")]  //声明控制器的操作支持 application/json 的响应内容类型
    [Authorize(Roles = "Admin,Engineer,Operator,Guest")]    //角色权限管控
    public class JobTwoController : ControllerBase
    {
        private ScheduleManager scheduleManager;

        /// <summary>
        /// 
        /// </summary>
        public JobTwoController()
        {
            this.scheduleManager = new ScheduleManager();
        }

        /// <summary>
        /// 添加指定任务
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        // [CustomCacheResourceFilter]
        // [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        // [ServiceFilter(typeof(CustomLogActionFilterAttribute))]
        // [TypeFilter(typeof(CustomExceptionFilterAttribute))]       
        public async Task<IActionResult> AddJob([FromForm] string jobGroup, [FromForm] string jobName, [FromForm] string _crom)
        {
            ApiResponseResultModel response = new ApiResponseResultModel();
            try
            {
                var re = await scheduleManager.AddScheduleJobAsync(jobGroup, jobName, _crom);
                response.State = re;
                if (re)
                    response.Message = $"{jobGroup}-{jobName} 任务添加成功！";
                else
                    response.Message = $"{jobGroup}-{jobName} 任务添加失败！";
                await Task.Delay(10);
            }
            catch (Exception ex)
            {
                response.State = false;
                response.Message = "system Error: " + ex.ToString();
            }
            return Content(JsonConvert.SerializeObject(response));
        }

        /// <summary>
        /// 暂停指定任务
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> PauseJob([FromForm] string jobGroup, [FromForm] string jobName)
        {
            ApiResponseResultModel response = new ApiResponseResultModel();
            try
            {
                var re = await scheduleManager.StopScheduleJobAsync(jobGroup, jobName);
                response.State = re;
                if (re)
                    response.Message = $"{jobGroup}-{jobName} 任务暂停成功！";
                else
                    response.Message = $"{jobGroup}-{jobName} 任务暂停失败！";
                await Task.Delay(10);
            }
            catch (Exception ex)
            {
                response.State = false;
                response.Message = "system Error: " + ex.ToString();
            }
            return Content(JsonConvert.SerializeObject(response));
        }

        /// <summary>
        /// 恢复 暂停的任务
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> ResumeJob([FromForm] string jobGroup, [FromForm] string jobName)
        {

            ApiResponseResultModel response = new ApiResponseResultModel();
            try
            {
                var re = await scheduleManager.ResumeJobAsync(jobGroup, jobName);
                response.State = re;
                if (re)
                    response.Message = $"{jobGroup}-{jobName} 任务恢复成功！";
                else
                    response.Message = $"{jobGroup}-{jobName} 任务恢复失败！";
                await Task.Delay(10);
            }
            catch (Exception ex)
            {
                response.State = false;
                response.Message = "system Error: " + ex.ToString();
            }
            return Content(JsonConvert.SerializeObject(response));
        }
        //      0/3 * * * * ? 
        /// <summary>
        /// 删除指定的任务
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> StopJob([FromForm] string jobGroup, [FromForm] string jobName)
        {
            ApiResponseResultModel response = new ApiResponseResultModel();
            try
            {
                var re = await scheduleManager.StopScheduleJobAsync(jobGroup, jobName, true);
                response.State = re;
                if (re)
                    response.Message = $"{jobGroup}-{jobName} 任务删除成功！";
                else
                    response.Message = $"{jobGroup}-{jobName} 任务删除失败！";
                await Task.Delay(10);
            }
            catch (Exception ex)
            {
                response.State = false;
                response.Message = "system Error: " + ex.ToString();
            }
            return Content(JsonConvert.SerializeObject(response));


        }

    }


}
