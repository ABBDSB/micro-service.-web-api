﻿

namespace MicroService.WebApi.Controllers.V3
{

    /// <summary>
    /// 公司资源
    /// </summary>
    [ApiController]
    //[ApiVersion("3.0")]
    [Route("[controller]/[action]")]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = nameof(ApiVersions.V3))]  //自定义API版本
    [Produces("application/json")]  //声明控制器的操作支持 application/json 的响应内容类型
    [Authorize(Roles = "Admin,Engineer,Operator,Guest")]    //角色权限管控
    public class DBController : ControllerBase
    {
        private readonly ISqlSugarClient db;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="db"></param>
        public DBController(ISqlSugarClient db)
        {
            this.db = db;
        }

        /// <summary>
        /// 创建数据库
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        // [CustomCacheResourceFilter]
        [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        [AllowAnonymous]
        public async Task<IActionResult> aCreateDatabase([FromForm] string PWKey)
        {
            if (PWKey != "110")
            {
                var result2 = JsonConvert.SerializeObject(new ApiResponseResultModel()
                {
                    State = false,
                    Message = "秘钥不正确！",
                    Data = ""
                });
                await Task.Delay(10);
                return Content(result2);
            }
            var re = db.DbMaintenance.CreateDatabase();
            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = re,
                Message = re ? "数据库创建成功！" : "数据库创建失败！",
                Data = ""
            });
            await Task.Delay(10);
            return Content(result);
        }

        /// <summary>
        /// 创建数据表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        // [CustomCacheResourceFilter]
        [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        [AllowAnonymous]
        public async Task<IActionResult> aCreateUserTable([FromForm] string PWKey)
        {
            if (PWKey != "110")
            {
                var result2 = JsonConvert.SerializeObject(new ApiResponseResultModel()
                {
                    State = false,
                    Message = "秘钥不正确！",
                    Data = ""
                });
                await Task.Delay(10);
                return Content(result2);
            }

            // var re= db.Deleteable<UserModel>().ExecuteCommand();    // Delete Table  删除表之前需要查看是否存在该表
            db.CodeFirst.InitTables<UserModel>();                   // Create Table

            #region 插入数据
            List<UserModel> _userList = new List<UserModel>();
            _userList.Add(new UserModel()
            {
                Id = 1,
                Account = "teacher",
                Email = "123456@qq.com",
                Name = "teacher",
                Password = "123456",
                LoginTime = DateTime.Now,
                Roles = Role.Engineer.ToString()
            });
            _userList.Add(new UserModel()
            {
                Id = 2,
                Account = "root",
                Email = "88888@qq.com",
                Name = "root",
                Password = "123456",
                LoginTime = DateTime.Now,
                Roles = $"{Role.Operator},{Role.Admin},{Role.Engineer},{Role.Guest}"
            });
            _userList.Add(new UserModel()
            {
                Id = 3,
                Account = "student",
                Email = "138138438@qq.com",
                Name = "student",
                Password = "123456",
                LoginTime = DateTime.Now,
                Roles = Role.Operator.ToString()

            });
            _userList.Add(new UserModel()
            {
                Id = 4,
                Account = "admin",
                Email = "138138438@qq.com",
                Name = "admin",
                Password = "123456",
                LoginTime = DateTime.Now,
                Roles = Role.Admin.ToString()
            });
            var re = db.Insertable<UserModel>(_userList).ExecuteCommand(); // 插入数据
            #endregion

            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = re != 0,
                Message = re != 0 ? "User 表创建成功！" : "User 表创建失败！",
                Data = ""
            });
            await Task.Delay(10);
            return Content(result);
        }



        /// <summary>
        /// 获取所有用户信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        // [CustomCacheResourceFilter]
        [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        public async Task<IActionResult> GetAllUserInfo()
        {
            var list = db.Queryable<UserModel>().ToList();
            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = true,
                Message = "",
                Data = list
            });
            await Task.Delay(10);
            return Content(result);
        }

        /// <summary>
        /// 新增用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        // [CustomCacheResourceFilter]
        [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        public async Task<IActionResult> AddUser([FromForm] UserModel user)
        {
            var re = db.Insertable(user).ExecuteCommand();
            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = re != 0,
                Message = re != 0 ? "新增用户成功！" : "新增用户失败！",
                Data = ""
            });
            await Task.Delay(10);
            return Content(result);
        }

        /// <summary>
        /// 按姓名或账号查找用户信息
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="Account"></param>
        /// <returns></returns>
        [HttpPost]
        // [CustomCacheResourceFilter]
        [TypeFilter(typeof(CustomLogActionFilterAttribute))]
        public async Task<IActionResult> QueryUserByNameOrAccount([FromForm] string Name, [FromForm] string Account =null)
        {
            var re = db.Queryable<UserModel>().First(it => it.Name == Name || it.Account == Account);

            var result = JsonConvert.SerializeObject(new ApiResponseResultModel()
            {
                State = re != null,
                Message = re == null ? " 没查到" : "",
                Data = re
            });
            await Task.Delay(10);
            return Content(result);


        }



    }


}

