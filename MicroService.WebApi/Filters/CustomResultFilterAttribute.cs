﻿

namespace MicroService.WebApi.Filters
{
    /// <summary>
    /// 结果Filter
    /// 如果是在ASP.NET Core 中，还有应用场景：
    ///   有视图的概念，如果返回的视图；渲染视图数据---生成结果。Result就可以在视图渲染之前植入逻辑，在视图渲染之后，植入逻辑
    /// </summary>
    public class CustomResultFilterAttribute : Attribute, IResultFilter
    {
        /// <summary>
        /// 
        /// </summary>
        public void OnResultExecuted(ResultExecutedContext context)
        {
            Console.WriteLine("CustomResultFilterAttribute.OnResultExecuted");
        }

        /// <summary>
        /// 
        /// </summary>
        public void OnResultExecuting(ResultExecutingContext context)
        {
            Console.WriteLine("CustomResultFilterAttribute.OnResultExecuting");
        }
    }

    /// <summary>
    /// 结果Filter
    /// </summary>
    public class CustomAsyncResultFilterAttribute : Attribute, IAsyncResultFilter
    {
        /// <summary>
        /// 
        /// </summary>
        public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            Console.WriteLine("CustomAsyncResultFilterAttribute.OnResultExecutionAsync");
            {
                // 生成结果之前植入逻辑
            }
            await next.Invoke(); //生成结果
            {
                // 生成结果之后植入逻辑
            }
            //await Task.CompletedTask;
        }
    }

}
