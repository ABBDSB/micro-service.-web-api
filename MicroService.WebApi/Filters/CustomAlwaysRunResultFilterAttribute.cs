﻿

namespace MicroService.WebApi.Filters
{
    /// <summary>
    /// 结果 AlwaysRunResultFilter
    /// </summary>
    public class CustomAlwaysRunResultFilterAttribute : Attribute, IAlwaysRunResultFilter
    {
        /// <summary>
        /// 
        /// </summary>
        public void OnResultExecuted(ResultExecutedContext context)
        {
            Console.WriteLine("CustomAlwaysRunResultFilterAttribute.OnResultExecuted");
        }
        /// <summary>
        /// 
        /// </summary>
        public void OnResultExecuting(ResultExecutingContext context)
        {
            Console.WriteLine("CustomAlwaysRunResultFilterAttribute.OnResultExecuting");
        }
    }



}
