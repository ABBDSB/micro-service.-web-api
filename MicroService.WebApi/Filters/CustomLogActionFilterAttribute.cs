﻿

namespace MicroService.WebApi.Filters
{
    /// <summary>
    /// 自定义日志记录类
    /// ActionFilter 扩展
    /// 靠近API 方法，传入到API的参数，API执行结束后，执行结果都是 ActionFilter先获取
    /// 适合 日志记录
    /// </summary>
    public class CustomLogActionFilterAttribute : Attribute, IActionFilter
    {
        private readonly ILogger<CustomLogActionFilterAttribute> _logger;
        /// <summary>
        /// 构造 注入
        /// </summary>
        /// <param name="logger"></param>
        public CustomLogActionFilterAttribute(ILogger<CustomLogActionFilterAttribute> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// API方法执行前
        /// </summary>
        /// <param name="context"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void OnActionExecuting(ActionExecutingContext context)
        {
            var controllerName = context.RouteData.Values["controller"];
            var actionName = context.RouteData.Values["action"];
            var query= JsonConvert.SerializeObject(context.HttpContext.Request.Path);

            string remoteIpAddress = "";
            if (context.HttpContext.Connection.RemoteIpAddress != null)
            {
                remoteIpAddress = "Ip：" + context.HttpContext.Connection.RemoteIpAddress.ToString();
            }

            _logger.LogInformation($"{remoteIpAddress}_{controllerName}__{actionName} before,Command: {query}");
        }

        /// <summary>
        /// API方法执行后
        /// </summary>
        /// <param name="context"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void OnActionExecuted(ActionExecutedContext context)
        {
            var controllerName = context.RouteData.Values["controller"];
            var actionName = context.RouteData.Values["action"];
            //var query = JsonConvert.SerializeObject(context.HttpContext.Request.QueryString);
            var response = JsonConvert.SerializeObject(context.HttpContext.Response.StatusCode);
            
            string remoteIpAddress ="";
            if (context.HttpContext.Connection.RemoteIpAddress != null)
            {
                remoteIpAddress ="Ip："+ context.HttpContext.Connection.RemoteIpAddress.ToString();
            }

            _logger.LogInformation($"{remoteIpAddress}_{controllerName}__{actionName} after,StatusCode: {response}");
        }      
    }

    /// <summary>
    /// ActionFilter 异步版本扩展定制
    /// </summary>
    public class CustomAsyncActionFilterAttribute : Attribute, IAsyncActionFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            {
                //在API执行之前扩展业务逻辑
              
            }
            await next.Invoke();  // 这里就是执行API
            {
                //在API执行之后扩展业务逻辑

            }
        }
    }


}

/// <summary>
/// 请求日志拦截
/// </summary>
//public class RequestActionFilter : IAsyncActionFilter
//{
//    private readonly IEventPublisher _eventPublisher;

//    public RequestActionFilter(IEventPublisher eventPublisher)
//    {
//        _eventPublisher = eventPublisher;
//    }

//    public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
//    {
//        var httpContext = context.HttpContext;
//        var httpRequest = httpContext.Request;

//        var sw = new Stopwatch();
//        sw.Start();
//        var actionContext = await next();
//        sw.Stop();

//        // 判断是否请求成功（没有异常就是请求成功）
//        var isRequestSucceed = actionContext.Exception == null;
//        var headers = httpRequest.Headers;
//        var clientInfo = headers.ContainsKey("User-Agent") ? Parser.GetDefault().Parse(headers["User-Agent"]) : null;
//        var actionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
//        var isWriteLog = false;
//        var ip = Util.HttpNewUtil.Ip;
//        //判断是否需有禁用操作日志属性
//        foreach (var metadata in actionDescriptor.EndpointMetadata)
//        {
//            if (metadata.GetType() == typeof(OpLogAttribute))
//            {
//                isWriteLog = true;
//                break;
//            }
//        }
//        //请求异常时记录日志
//        if (!isRequestSucceed || App.GetOptions<SystemSettingsOptions>().IsGlobalRequestLog)
//        {
//            isWriteLog = true;
//        }
//        if (isWriteLog)
//        {
//            await _eventPublisher.PublishAsync(new ChannelEventSource("Create:OpLog", new SysOperateLogEntity
//            {
//                CreateBy = httpContext.User?.FindFirst(ClaimConst.CLAINM_USERID).Value,
//                OperateState = isRequestSucceed ? 1 : 0,
//                OperateIp = ip,
//                ActionName = actionDescriptor?.ActionName,
//                ControllerName = context.Controller.ToString(),
//                Parameter = JSON.Serialize(context.ActionArguments.Count < 1 ? "" : context.ActionArguments),
//                CreateDate = DateTime.Now,
//                OperateMsg = actionContext.Result?.GetType() == typeof(JsonResult) ? JSON.Serialize(actionContext.Result) : "",
//                ConsumedTime = sw.ElapsedMilliseconds
//            }));
//        }
//    }
//}

