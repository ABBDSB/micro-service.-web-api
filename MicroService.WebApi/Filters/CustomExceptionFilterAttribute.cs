﻿

namespace MicroService.WebApi.Filters
{
    /// <summary>
    /// 自定义异常处理类
    /// ExceptionFilter 扩展
    /// </summary>
    public class CustomExceptionFilterAttribute : Attribute, IExceptionFilter
    {
        private readonly ILogger<CustomExceptionFilterAttribute> _logger;

        /// <summary>
        /// 构造 注入
        /// </summary>
        /// <param name="logger"></param>
        public CustomExceptionFilterAttribute(ILogger<CustomExceptionFilterAttribute> logger)
        {
            this._logger = logger;
        }

        /// <summary>
        /// 当有异常发生的时候，触发这个方法
        /// </summary>
        /// <param name="context"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void OnException(ExceptionContext context)
        {

            var controllerName = context.RouteData.Values["controller"];
            var actionName = context.RouteData.Values["action"];

            _logger.LogError(controllerName+"__"+ actionName +", Error: "+  context.Exception.Message);
            
            //在这里就应该处理异常
            if (context.ExceptionHandled == false)// 如果异常没有被处理
            {
                context.Result = new JsonResult(new ApiResponseResultModel()
                {
                    State = false,
                    Message ="system Error: "+ context.Exception.Message
                });
                context.ExceptionHandled = true; //异常已经被处理过了
            }
        }
    }

    /// <summary>
    /// 自定义异常处理类
    /// AsyncExceptionFilter 扩展
    /// </summary>
    public class CustomAsyncExceptionFilterAttribute : Attribute, IAsyncExceptionFilter
    {
        private readonly ILogger<CustomExceptionFilterAttribute> _logger;
        /// <summary>
        /// 构造注入
        /// </summary>
        /// <param name="logger"></param>
        public CustomAsyncExceptionFilterAttribute (ILogger<CustomExceptionFilterAttribute> logger)
        {
            this._logger = logger;
        }

        /// <summary>
        /// 当有异常发生的时候，触发这个方法
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task OnExceptionAsync(ExceptionContext context)
        {
            _logger.LogError(context.Exception.Message);

            //在这里就应该处理异常
            if (context.ExceptionHandled == false)// 如果异常没有被处理
            {
                
                context.Result = new JsonResult(new ApiResponseResultModel()
                {
                    State = false,
                    Message = context.Exception.Message
                });
                context.ExceptionHandled = true; //异常已经被处理过了
            }
            await Task.CompletedTask;
        }
    }



}
