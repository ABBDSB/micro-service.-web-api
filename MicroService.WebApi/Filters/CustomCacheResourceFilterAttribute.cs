﻿

namespace MicroService.WebApi.Filters
{
    /// <summary>
    /// 自定义缓存类
    /// ResourceFilter扩展  --适合做缓存，性能最高
    /// 可以用于 重复请求的方法   
    /// 但是 可能也会存在 问题：不能获取更新的数据，可能还在使用旧数据的情况
    /// </summary>
    public class CustomCacheResourceFilterAttribute : Attribute, IResourceFilter
    {
        /// <summary>
        /// 缓存区域
        /// </summary>
        private static Dictionary<string, object> CacheDictionary = new Dictionary<string, object>();

        /// <summary>
        /// API方法执行之后
        /// </summary>
        /// <param name="context"></param>
        public void OnResourceExecuted(ResourceExecutedContext context)
        {
            //如果能够执行到这里，说明一定已经执行了，控制器构造函数 一定已经执行了API了
            //必然也已经得到了计算的结果了；就应该把计算的结果保存到缓存中去

            string key = context.HttpContext.Request.Path; //Url地址
            CacheDictionary[key] = context.Result;
        }

        /// <summary>
        /// API方法执行之前
        /// </summary>
        /// <param name="context"></param>
        public void OnResourceExecuting(ResourceExecutingContext context)
        {           
            string key = context.HttpContext.Request.Path;
            
            //在这里检查一下缓存，如果存在就直接返回 
            if (CacheDictionary.ContainsKey(key))
            {
                object oResult = CacheDictionary[key];
                IActionResult result = oResult as IActionResult;
                context.Result = result; //请求处理的过程中的一个短路器，如果Result
            }
        }
    }
}
