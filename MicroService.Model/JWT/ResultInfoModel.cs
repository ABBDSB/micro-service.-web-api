﻿namespace MicroService.Entity
{

    /// <summary>
    /// Api返回结果类
    /// </summary>
    public class ApiResponseResultModel
    {
        /// <summary>
        /// 是否成功状态标记 true/false
        /// </summary>
        public bool State { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public string? Message { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public object? Data { get; set; }

    }
}
