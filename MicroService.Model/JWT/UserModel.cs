﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Entity
{
    /// <summary>
    /// 用户信息
    /// </summary>
    [SugarTable]
    public class UserModel
    {
        /// <summary>
        /// ID
        /// </summary>
        [SugarColumn(IsIdentity = true, IsPrimaryKey = true)]
        public int Id { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        [SugarColumn(ColumnDataType = "Nvarchar(255)")]
        public string Name { get; set; }
        /// <summary>
        /// 账号/工号
        /// </summary>
        [SugarColumn(ColumnDataType = "Nvarchar(255)")]
        public string Account { get; set; }
        /// <summary>
        /// 登录密码
        /// </summary>
        [SugarColumn(ColumnDataType = "Nvarchar(255)")]
        public string Password { get; set; }


        /// <summary>
        /// 角色
        /// </summary>
        [SugarColumn(ColumnDataType = "Nvarchar(255)")]        
        public string Roles { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string? Email { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string? PhoneNumber { get; set; }
        /// <summary>
        /// 最近登录时间
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public DateTime? LoginTime { get; set; }

    }
}
