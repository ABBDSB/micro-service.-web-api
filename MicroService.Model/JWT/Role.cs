﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Entity
{
    /// <summary>
    /// 角色定义
    /// </summary>
    public enum Role
    {
        /// <summary>
        /// 系统管理员
        /// </summary>
        Admin,
        /// <summary>
        /// 工程师
        /// </summary>
        Engineer,
        /// <summary>
        /// 普通员工
        /// </summary>
        Operator,
        /// <summary>
        /// 临时权限账号
        /// </summary>
        Guest
    }
}
