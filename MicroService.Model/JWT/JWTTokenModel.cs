﻿using System;

namespace MicroService.Entity
{
    /// <summary>
    /// 用于接受配置数据实体类型
    /// </summary>
    public class JWTTokenModel
    {
        /// <summary>
        /// 获取或设置接受者
        /// </summary>
        public string? Audience { get; set; }

        /// <summary>
        /// 获取或设置加密 Key
        /// </summary>
        public string? SecurityKey { get; set; }

        /// <summary>
        /// 获取或设置发布者
        /// </summary>
        public string? Issuer { get; set; }
    }
}
