﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Quartz.CustomListener
{
    /// <summary>
    /// 调度器Scheduler 监听
    /// </summary>
    public class CustomSchedulerListener : ISchedulerListener
    {
        public async Task JobAdded(IJobDetail jobDetail, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is JobAdded");
            });
        }

        public async Task JobDeleted(JobKey jobKey, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is JobDeleted");
            });
        }

        public async Task JobInterrupted(JobKey jobKey, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is JobInterrupted");
            });
        }

        public async Task JobPaused(JobKey jobKey, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is JobPaused");
            });
        }

        public async Task JobResumed(JobKey jobKey, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is JobResumed");
            });
        }

        //用于部署JobDetail时调用
        public async Task JobScheduled(ITrigger trigger, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is JobScheduled");
            });
        }
        //当一个或一组 JobDetail 暂停时调用这个方法。
        public async Task JobsPaused(string jobGroup, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is JobsPaused");
            });
        }
        //当一个或一组 Job 从暂停上恢复时调用这个方法。假如是一个 Job 组，jobName 参数将为 null。
        public async Task JobsResumed(string jobGroup, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is JobsResumed");
            });
        }
        //用于卸载JobDetail时调用
        public async Task JobUnscheduled(TriggerKey triggerKey, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is JobUnscheduled");
            });
        }

        /// <summary>
        /// 在 Scheduler 的正常运行期间产生一个严重错误时调用这个方法。
        /// </summary>
        public async Task SchedulerError(string msg, SchedulerException cause, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is SchedulerError");
            });
        }
        /// <summary>
        ///  当Scheduler处于StandBy模式时，调用该方法
        /// </summary>
        public async Task SchedulerInStandbyMode(CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is SchedulerInStandbyMode");
            });
        }
        /// <summary>
        /// 当Scheduler停止时，调用该方法
        /// </summary>
        public async Task SchedulerShutdown(CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is SchedulerShutdown");
            });
        }

        public async Task SchedulerShuttingdown(CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is SchedulerShuttingdown");
            });
        }
        /// <summary>
        /// 当Scheduler 开启时，调用该方法
        /// </summary>
        public async Task SchedulerStarted(CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is SchedulerStarted");
            });
        }

        public async Task SchedulerStarting(CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is SchedulerStarting");
            });
        }
        /// <summary>
        /// 当Scheduler中的数据被清除时，调用该方法。
        /// </summary>
        public async Task SchedulingDataCleared(CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is SchedulingDataCleared");
            });
        }

        /// <summary>
        ///  当一个 Trigger 来到了再也不会触发的状态时调用这个方法。
        ///  除非这个 Job 已设置成了持久性，否则它就会从 Scheduler 中移除。
        /// </summary>
        public async Task TriggerFinalized(ITrigger trigger, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is TriggerFinalized");
            });
        }

        /// <summary>
        /// Scheduler 调用这个方法是发生在一个 Trigger 或 Trigger 组被暂停时。
        /// 假如是 Trigger 组的话，triggerName 参数将为 null。
        /// </summary>
        public async Task TriggerPaused(TriggerKey triggerKey, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is TriggerPaused");
            });
        }

        /// <summary>
        /// Scheduler 调用这个方法是发生成一个 Trigger 或 Trigger 组从暂停中恢复时。
        /// 假如是 Trigger 组的话，假如是 Trigger 组的话，triggerName 参数将为 null。参数将为 null。
        /// </summary>
        public async Task TriggerResumed(TriggerKey triggerKey, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is TriggerResumed");
            });
        }

        public async Task TriggersPaused(string? triggerGroup, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is TriggersPaused");
            });
        }

        public async Task TriggersResumed(string? triggerGroup, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is TriggersResumed");
            });
        }
    }
}
