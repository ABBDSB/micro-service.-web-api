﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Quartz.CustomListener
{
    /// <summary>
    /// 触发器Trigger 监听
    /// </summary>
    public class CustomTriggerListener : ITriggerListener
    {
        public string Name => "CustomTriggerListener";  //用于获取触发器的名称

        //Trigger 被触发并且完成了 Job 的执行时，Scheduler 调用这个方法。
        public async Task TriggerComplete(ITrigger trigger, IJobExecutionContext context, SchedulerInstruction triggerInstructionCode, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is TriggerComplete");
            });
        }

        //当与监听器相关联的Trigger被触发，Job上的**execute()**方法将被执行时，Scheduler就调用该方法
        public async Task TriggerFired(ITrigger trigger, IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is TriggerFired");
            });
        }

        //Scheduler 调用这个方法是在 Trigger 错过触发时。你应该关注此方法中持续时间长的逻辑：在出现许多错过触发的 Trigger 时，长逻辑会导致骨牌效应，所以应当保持这方法尽量的小
        public async Task TriggerMisfired(ITrigger trigger, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is TriggerMisfired");
            });
        }

        /// <summary>
        /// 结果返回false 就继续执行； 结果返回true 就被取消了
        /// 在 Trigger 触发后，Job 将要被执行时由 Scheduler 调用这个方法。TriggerListener 给了一个选择去否决 Job 的执行。假如这个方法返回 true，这个 Job 将不会为此次 Trigger 触发而得到执行
        /// </summary>
        public async Task<bool> VetoJobExecution(ITrigger trigger, IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is JobExecutionVetoed");
            });
            return false;
        }
    }
}
