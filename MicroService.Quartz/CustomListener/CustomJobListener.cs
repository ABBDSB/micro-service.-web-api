﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Quartz.CustomListener
{
    /// <summary>
    /// 任务Job监听 一般使用这个 可以做一些日志的记录
    /// </summary>
    public class CustomJobListener : IJobListener
    {
        public string Name => "CustomJobListener";  //用于获取改JobListener 的名称

        //Scheduler 在 JobDetail 即将被执行，但又被 TriggerListener 否决时会调用该方法
        public async Task JobExecutionVetoed(IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is JobExecutionVetoed");
            });
        }

        //Scheduler 在 JobDetail 将要被执行时调用这个方法
        public async Task JobToBeExecuted(IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is JobToBeExecuted");
            });
        }

        //Scheduler 在 JobDetail 被执行之后调用这个方法
        public async Task JobWasExecuted(IJobExecutionContext context, JobExecutionException? jobException, CancellationToken cancellationToken = default)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("This is JobWasExecuted");
            });
        }
    }
}
