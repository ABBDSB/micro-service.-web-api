﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Quartz.CustomJob
{
    [DisallowConcurrentExecution] //不允许当前执行，需确认上次执行完毕后再往后执行
    public class SayHiJob : IJob
    {
        public SayHiJob()
        {
            Console.WriteLine("SayHiJob被构造了");
        }

        public async Task Execute(IJobExecutionContext context)
        {
            await Task.Run(() =>
            {
                
                Console.WriteLine();
                Console.WriteLine($"************** {DateTime.Now} *****************");
                Console.WriteLine();
            });
        }
    }
}
