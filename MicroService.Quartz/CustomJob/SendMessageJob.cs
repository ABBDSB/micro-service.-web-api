﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Quartz.CustomJob
{
    [PersistJobDataAfterExecution] //执行后的保留作业数据，标记特性+JobDataMap.Put实现链式传参
    [DisallowConcurrentExecution] //不允许当前执行，需确认上次执行完毕后再往后执行
    public class SendMessageJob : IJob //(无状态)
    {
        public SendMessageJob()
        {
            Console.WriteLine("SendMessageJob被构造了");
        }

        /// <summary>
        /// 通过静态对象也可以实现链式传参
        /// </summary>
        private static object tempObject = new object();

        /// <summary>
        ///  当前Task内部就作业需要执行的任务
        /// </summary>       
        public async Task Execute(IJobExecutionContext context)
        {
            await Task.Run(() =>
            {
                //System.Threading.Thread.Sleep(6000); //如果这个方法比较耗时

                Console.WriteLine($"--------------------------------------------");

                var student1 = context.JobDetail.JobDataMap.GetString("student1");
                var student2 = context.JobDetail.JobDataMap.Get("student2");
                var student3 = context.JobDetail.JobDataMap.Get("student3");

                var Year = context.JobDetail.JobDataMap.GetInt("Year");

                Console.WriteLine($"------  {student1} {DateTime.Now}--------------");
                Console.WriteLine($"------  {student2} {DateTime.Now}--------------");
                Console.WriteLine($"------  {student3} {DateTime.Now}--------------");
                Console.WriteLine($"------  {Year} {DateTime.Now}--------------");


                var student4 = context.Trigger.JobDataMap.Get("student4");
                var student5 = context.Trigger.JobDataMap.Get("student5");
                var student6 = context.Trigger.JobDataMap.Get("student6");
                var Year2 = context.Trigger.JobDataMap.GetInt("Year2");

                Console.WriteLine($"------  {student4} {DateTime.Now}--------------");
                Console.WriteLine($"------  {student5} {DateTime.Now}--------------");
                Console.WriteLine($"------  {student6} {DateTime.Now}--------------");
                Console.WriteLine($"------  {Year2} {DateTime.Now}--------------");

                var Year3 = context.MergedJobDataMap.Get("Year");
                Console.WriteLine($"Year3:{Year3}");

                context.JobDetail.JobDataMap.Put("Year", Year + 1); //需配合[PersistJobDataAfterExecution]特性标记

                Console.WriteLine("async Task Execute");
                Console.WriteLine($"--------------------------------------------");

            });

        }
    }
}
