﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Quartz.CustomJob
{
    [DisallowConcurrentExecution] //不允许当前执行，需确认上次执行完毕后再往后执行
    public class ApiJob : IJob
    {

        public async Task Execute(IJobExecutionContext context)
        {

            //var ss = context.JobDetail


            await Task.Run(() =>
            {
                Console.WriteLine();
                Console.WriteLine($"************** __ApiJob___ {DateTime.Now} *****************");
                Console.WriteLine();
            });
        }
    }
}
