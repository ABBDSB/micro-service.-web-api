﻿using MicroService.Quartz.CustomJob;
using MicroService.Quartz.CustomListener;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Quartz
{
    /*先需引用Quartz包
     */
    public class DispatchingManager
    {
        
        public async static Task Init()
        {
            //  创建单元(时间轴/载体)
            StdSchedulerFactory factory = new StdSchedulerFactory();
            IScheduler scheduler = await factory.GetScheduler();
            await scheduler.Start();

            // Job作业
            IJobDetail jobDetail = JobBuilder.Create<SendMessageJob>()
                .WithIdentity("sendMessageJob","group1")
                .WithDescription("This is Send Message Job")
                .Build();

            //Trigger时间策略
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("sendMessageTrigger","group1")
                .StartNow() //立刻执行一次
                .WithSimpleSchedule(w=>w.WithIntervalInSeconds(5).WithRepeatCount(3))  //每5s执行一次，总共执行3次
                .Build();

            //把时间策略和作业承载到单元上
            await scheduler.ScheduleJob(jobDetail, trigger);
        }

        public async static Task Init2()
        {
            //  创建单元(时间轴/载体)
            StdSchedulerFactory factory = new StdSchedulerFactory();
            IScheduler scheduler = await factory.GetScheduler();
            await scheduler.Start();

            // Job作业
            IJobDetail jobDetail = JobBuilder.Create<SendMessageJob>()
                .WithIdentity("sendMessageJob", "group1")
                .WithDescription("This is Send Message Job")
                .Build();
            jobDetail.JobDataMap.Add("student1", "张三"); //传入参数
            jobDetail.JobDataMap.Add("student2", "李四"); //传入参数
            jobDetail.JobDataMap.Add("student3", "王五"); //传入参数
            jobDetail.JobDataMap.Add("Year", DateTime.Now.Year); //传入参数


            //Trigger时间策略
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("sendMessageTrigger", "group1")
                .StartNow() //立刻执行一次
                .WithSimpleSchedule(w => w.WithIntervalInSeconds(5).WithRepeatCount(3))  //每5s执行一次，总共执行3次
                .Build();
            trigger.JobDataMap.Add("student4", "赵六"); //传入参数
            trigger.JobDataMap.Add("student5", "田七"); //传入参数
            trigger.JobDataMap.Add("student6", "王八"); //传入参数
            trigger.JobDataMap.Add("Year2", DateTime.Now.Year-1); //传入参数

            //把时间策略和作业承载到单元上
            await scheduler.ScheduleJob(jobDetail, trigger);
        }

        public async static Task Init3()
        {
            //  创建单元(时间轴/载体)
            StdSchedulerFactory factory = new StdSchedulerFactory();
            IScheduler scheduler = await factory.GetScheduler();
            await scheduler.Start();


            //Trigger时间策略
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("sendMessageTrigger", "group1")
                //.StartAt(new DateTimeOffset()) //从某一个时间点开始
                //.StartNow() //马上执行一次
                .WithSimpleSchedule(w => w.WithIntervalInSeconds(2)
                .WithRepeatCount(10) //最多执行10次
                //.RepeatForever()
                )
                .Build();
           
            //Job作业
            IJobDetail jobDetail = JobBuilder.Create<SayHiJob>()
                .WithIdentity("SayHiJob", "group1")
                .WithDescription("This is Send SayHi Job")
                .Build();

            //把时间策略和作业承载到单元上
            await scheduler.ScheduleJob(jobDetail, trigger);
        }

        public async static Task Init4()
        {
            //  创建单元(时间轴/载体)
            StdSchedulerFactory factory = new StdSchedulerFactory();
            IScheduler scheduler = await factory.GetScheduler();
            await scheduler.Start();


            //Trigger时间策略---- Cron
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("sendMessageTrigger", "group1")
                //.StartAt(new DateTimeOffset(DateTime.Now.AddSeconds(10))) //从某一个时间点开始 如10秒后再执行
                .StartNow() //马上执行一次
                .WithCronSchedule("5/10 * * * * ?") //从5开始每隔开10秒中执行一次
                .WithDescription("This is testJob Trigger")
                .Build();

            //Job作业
            IJobDetail jobDetail = JobBuilder.Create<SayHiJob>()
                .WithIdentity("SayHiJob", "group1")
                .WithDescription("This is Send SayHi Job")
                .Build();

            //把时间策略和作业承载到单元上
            await scheduler.ScheduleJob(jobDetail, trigger);
        }


        public async static Task Init5()
        {
            //  创建单元(时间轴/载体)
            StdSchedulerFactory factory = new StdSchedulerFactory();
            IScheduler scheduler = await factory.GetScheduler();
            await scheduler.Start();

            //JobListener
            scheduler.ListenerManager.AddJobListener(new CustomJobListener());

            //Trigger时间策略
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("sendMessageTrigger", "group1")
                //.StartAt(new DateTimeOffset(DateTime.Now.AddSeconds(10))) //从某一个时间点开始
                .StartNow() //马上执行一次
                            //.WithCronSchedule("5/10 * * * * ?") //从5开始每隔开10秒中执行一次
                .WithSimpleSchedule(w => w.WithIntervalInSeconds(5).WithRepeatCount(5))
                .WithDescription("This is testJob Trigger")
                .Build();

            //Job作业
            IJobDetail jobDetail = JobBuilder.Create<SayHiJob>()
                .WithIdentity("SayHiJob", "group1")
                .WithDescription("This is Send SayHi Job")
                .Build();

            //把时间策略和作业承载到单元上
            await scheduler.ScheduleJob(jobDetail, trigger);
        }

        public async static Task Init6()
        {
            //  创建单元(时间轴/载体)
            StdSchedulerFactory factory = new StdSchedulerFactory();
            IScheduler scheduler = await factory.GetScheduler();
            await scheduler.Start();

            //JobListener
            scheduler.ListenerManager.AddTriggerListener(new CustomTriggerListener());


            //Trigger时间策略
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("sendMessageTrigger", "group1")
                //.StartAt(new DateTimeOffset(DateTime.Now.AddSeconds(10))) //从某一个时间点开始
                .StartNow() //马上执行一次
                            //.WithCronSchedule("5/10 * * * * ?") //从5开始每隔开10秒中执行一次
                .WithSimpleSchedule(w => w.WithIntervalInSeconds(5).WithRepeatCount(5))
                .WithDescription("This is testJob Trigger")
                .Build();

            //Job作业
            IJobDetail jobDetail = JobBuilder.Create<SayHiJob>()
                .WithIdentity("SayHiJob", "group1")
                .WithDescription("This is Send SayHi Job")
                .Build();

            //把时间策略和作业承载到单元上
            await scheduler.ScheduleJob(jobDetail, trigger);
        }

        public async static Task Init7()
        {
            //  创建单元(时间轴/载体)
            StdSchedulerFactory factory = new StdSchedulerFactory();
            IScheduler scheduler = await factory.GetScheduler();
            await scheduler.Start();

            //JobListener
            scheduler.ListenerManager.AddSchedulerListener(new CustomSchedulerListener());


            //Trigger时间策略
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("sendMessageTrigger", "group1")
                //.StartAt(new DateTimeOffset(DateTime.Now.AddSeconds(10))) //从某一个时间点开始
                .StartNow() //马上执行一次
                            //.WithCronSchedule("5/10 * * * * ?") //从5开始每隔开10秒中执行一次
                .WithSimpleSchedule(w => w.WithIntervalInSeconds(5).WithRepeatCount(5))
                .WithDescription("This is testJob Trigger")
                .Build();

            //Job作业
            IJobDetail jobDetail = JobBuilder.Create<SayHiJob>()
                .WithIdentity("SayHiJob", "group1")
                .WithDescription("This is Send SayHi Job")
                .Build();

            //把时间策略和作业承载到单元上
            await scheduler.ScheduleJob(jobDetail, trigger);
        }

    }
}


/*
 常用时间策略
 1、SimpleTrigger:  从什么时间开始，间隔多久执行重复操作
                    可以限制最大次数

 2、Cron：表达式的方式，可以灵活订制时间规则
 

 监听器和日志
  1、 SchedulerListener

  2、 TriggerListener

  3、 JobListener

 */