﻿using MicroService.Quartz.CustomJob;
using MicroService.Quartz.CustomListener;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.AdoJobStore;
using Quartz.Simpl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Quartz
{
    /// <summary>
    /// 调度中心[单例]
    /// </summary>
    public class ScheduleManager
    {
        private static IScheduler scheduler;

        /// <summary>
        /// 初始化Scheduler
        /// </summary>
        /// <returns></returns>
        private async Task InitSchedulerAsync()
        {
            if (scheduler == null)
            {
                scheduler = await BuildScheduler();
               
            }         
        }

        private async Task<IScheduler> BuildScheduler()
        {
            var properties = new NameValueCollection();
            properties["quartz.scheduler.instanceName"] = "后台作业任务";

            //设置线程池
            properties["quartz.threadPool.type"] = "Quartz.Simpl.SimpleThreadPool, Quartz";
            properties["quartz.threadPool.threadCount"] = "5";
            properties["quartz.threadPool.threadPriority"] = "Normal";

            StdSchedulerFactory schedulerFactory = new StdSchedulerFactory(properties);
            IScheduler _scheduler = await schedulerFactory.GetScheduler();
            return _scheduler;
        }

        /// <summary>
        /// 创建Job作业
        /// </summary>
        private IJobDetail CreateIJobDetail( string jobGroup, string jobName )
        {
          
            return JobBuilder.Create<ApiJob>()
                    .WithIdentity(jobName, jobGroup)
                    .Build();
        }
        /// <summary>
        /// 创建Trigger时间策略
        /// </summary>
        private ITrigger CreateITrigger(string jobGroup, string jobName, string cron)
        {
            return TriggerBuilder.Create()
                 .WithIdentity(jobName, jobGroup)
                 //.StartAt(new DateTimeOffset(DateTime.Now.AddSeconds(10))) //从某一个时间点开始
                 .StartNow() //马上执行一次
                 .WithCronSchedule(cron)
                 .Build();
        }


        /// <summary>
        /// 开启调度器
        /// </summary>
        /// <returns></returns>
        private async Task<bool> StartScheduleAsync()
        {           
            //初始化Scheduler
            await InitSchedulerAsync();
            //开启调度器
            if (scheduler.InStandbyMode)
            {
                await scheduler.Start();
                Console.WriteLine("任务调度启动！");
            }
            return scheduler.InStandbyMode;
        }


        /// <summary>
        /// 添加一个任务调度
        /// </summary>
        /// <param name="jobGroup">任务分组</param>
        /// <param name="jobName">任务名称</param>
        /// <param name="_cron">任务调度时间规则表达式</param>
        /// <returns></returns>
        public async Task<bool> AddScheduleJobAsync(string jobGroup, string jobName, string _cron)
        {
            try
            {
                await StartScheduleAsync();

                //Trigger时间策略
                ITrigger trigger = CreateITrigger(jobGroup, jobName, _cron);

                //Job作业
                IJobDetail jobDetail = CreateIJobDetail(jobGroup, jobName); ;

                //把时间策略和作业承载到单元上
                await scheduler.ScheduleJob(jobDetail, trigger);
            }
            catch(Exception ex)
            {
                return false;
            }

            Console.WriteLine($"AddScheduleJobAsync:{jobGroup}_{jobName}_[{_cron}]");
            return true;
        }


        /// <summary>
        /// 暂停/删除 指定的任务[Job]
        /// </summary>
        /// <param name="jobGroup">任务分组</param>
        /// <param name="jobName">任务名称</param>
        /// <param name="isDelete">停止并删除任务</param>
        /// <returns></returns>
        public async Task<bool> StopScheduleJobAsync(string jobGroup,string jobName, bool isDelete = false)
        {
            bool result = false;
            try
            {
                await scheduler.PauseJob(new JobKey(jobName, jobGroup));
                if(isDelete)
                {
                    await scheduler.DeleteJob(new JobKey(jobName, jobGroup));
                }
                result = true;
            }
            catch(Exception ex)
            {
                result = false;
            }
            Console.WriteLine($"StopScheduleJobAsync:{jobGroup}_{jobName}");
            return result;
        }


        /// <summary>
        /// 恢复运行暂停的任务
        ///  问题： 恢复暂停的任务时，会出现暂停时段没执行的数次任何 会在恢复暂停后全部陆续执行(没按时间策略，直到耽误的次数执行完成后才按时间策略执行)
        /// </summary>
        /// <param name="jobGroup">任务分组</param>
        /// <param name="jobName">任务名称</param>
        /// <returns></returns>
        public async Task<bool> ResumeJobAsync(string jobGroup, string jobName)
        {
            bool result = false;
            try
            {
               var jobKey = new JobKey(jobName,jobGroup);
                if(await scheduler.CheckExists(jobKey))
                {
                    var jobDetail = await scheduler.GetJobDetail(jobKey);
                    var endTime = jobDetail.JobDataMap.GetString("EndAt");
                    if(!string.IsNullOrWhiteSpace(endTime) && DateTime.Parse(endTime) <= DateTime.Now)
                    {
                        // Job 的结束时间已过期                      
                    }
                    else
                    {
                        result = true;
                        await scheduler.ResumeJob(jobKey);
                    }
                }
                else
                {                  
                    // 任务不存在
                }              
            }
            catch (Exception ex)
            {
                result = false;
            }
            Console.WriteLine($"ResumeJobAsync:{jobGroup}_{jobName}");
            return result;
        }


        /// <summary>
        /// 立即执行
        /// </summary>
        /// <param name="jobKey"></param>
        /// <returns></returns>
        public async Task<bool> TriggerJobAsync(JobKey jobKey)
        {
            await scheduler.TriggerJob(jobKey);
            return true;
        }

     
        public async Task<bool> StopScheduleAsync()
        {
            //判断调度是否已经关闭
            if(!scheduler.InStandbyMode)
            {
                //等待任务运行完成
                await scheduler.Standby(); // Shutdown后Start会报错，所以这里使用暂停
            }
            return !scheduler.InStandbyMode;
        }



    }
}
